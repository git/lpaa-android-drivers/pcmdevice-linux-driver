/*
 * pcmdevice
 *
 * pcmdevice codec driver
 *
 * Copyright (C) 2020-2024 Texas Instruments Incorporated
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/firmware.h>
#include <linux/i2c.h>
#include <linux/module.h>
#include <linux/uaccess.h>
#include <linux/version.h>
#include <sound/soc.h>

#include "pcmdevice.h"
#include "pcmdevice-node.h"
#include "pcmdevice-rw.h"

#define PCMDEVICE_PAGE_MAX	(255)
extern const char *blocktype[5];

static char gSysCmdLog[MaxCmd][256];

ssize_t i2c_address_show(struct device *dev,
	struct device_attribute *attr, char *buf)
{
	struct pcmdevice_priv *pcm_dev = dev_get_drvdata(dev);
	const int size = 32;
	int n = 0;
	unsigned short addr;

	if (pcm_dev != NULL) {
		struct i2c_client *pClient =
			(struct i2c_client *)pcm_dev->client;
		addr = pClient->addr;
	}

	n += scnprintf(buf, size, "Active SmartPA-0x%02x\n\r", addr);

	return n;
}

ssize_t device_list_show(struct device *dev,
	struct device_attribute *attr, char *buf)
{
	struct pcmdevice_priv *pcm_dev = dev_get_drvdata(dev);
	int n = 0;
	int i;

	if (pcm_dev != NULL) {
		n += scnprintf(buf + n, 50,
			"DEV NO\tDEV NAME\t\tI2C addr\n\r");
		for (i = 0; i < pcm_dev->ndev; i++) {
			n += scnprintf(buf + n, 50,
				"%d\t\t%s\t0x%02x\n\r", i,
				pcm_dev->dev_name,
				pcm_dev->pcmdevice[i].addr);
		}
	}
	return n;
}

ssize_t reg_show(struct device *dev,
	struct device_attribute *attr, char *buf)
{
	struct pcmdevice_priv *pcm_dev = dev_get_drvdata(dev);
	ssize_t len = 0;
	const int size = PAGE_SIZE;
	int data = 0;
	int n_result = 0;

	if (pcm_dev != NULL) {
		struct Tsyscmd *pSysCmd = &pcm_dev->nSysCmd[RegSettingCmd];
		struct i2c_client *pClient =
			(struct i2c_client *)pcm_dev->client;
		if (pSysCmd->bCmdErr == true) {
			len += scnprintf(buf, pSysCmd->bufLen,
				gSysCmdLog[RegSettingCmd]);
			goto out;
		}
		//15 bytes
		len += scnprintf(buf+len, size-len, "i2c-addr: 0x%02x\n",
			pClient->addr);
		//2560 bytes

		n_result = pcmdevice_dev_read(pcm_dev, pSysCmd->mdev,
			PCMDEVICE_REG(pSysCmd->mnPage, pSysCmd->mnReg), &data);
		if (n_result < 0) {
			len += scnprintf(buf, size - len,
				"%s: read register failed\n",
				__func__);
			goto out;
		}
		//20 bytes
		if (len + 25 <= size) {
			len += scnprintf(buf+len, size-len,
				"dev-%d:P0x%02xR0x%02x:0x%02x\n",
				pSysCmd->mdev, pSysCmd->mnPage,
				pSysCmd->mnReg, data);
		} else {
			scnprintf(buf + PAGE_SIZE - 100, 100,
				"%s: mem is not enough, "
				"PAGE_SIZE = %lu\n",
				__func__, PAGE_SIZE);
			len = PAGE_SIZE;
		}

	}
out:
	return len;
}

ssize_t reg_store(struct device *dev,
	struct device_attribute *attr, const char *buf, size_t count)
{
	struct pcmdevice_priv *pcm_dev = dev_get_drvdata(dev);
	int ret = 0;
	unsigned char kbuf[4];
	char *temp = NULL;
	int n_result = 0;
	struct Tsyscmd *pSysCmd = NULL;


	if (pcm_dev == NULL)
		return count;
	dev_info(pcm_dev->dev, "reg: count = %d\n", (int)count);
	pSysCmd = &pcm_dev->nSysCmd[RegSettingCmd];
		pSysCmd->bufLen = snprintf(gSysCmdLog[RegSettingCmd], 256,
		"command: echo dev 0xPG 0xRG 0xXX > NODE\n"
		"dev is device no, should be 1-digital;"
			"PG, RG & XX must be 2-digital HEX\n"
			"eg: echo 0x00 0x2 0xE1 > NODE\n\r");

	if (count >= 12) {
		temp = kmalloc(count, GFP_KERNEL);
		if (!temp) {
			pSysCmd->bCmdErr = true;
			pSysCmd->bufLen += snprintf(gSysCmdLog[RegSettingCmd],
			15, "No memory!\n");
			goto out;
		}
		memcpy(temp, buf, count);
		ret = sscanf(temp, "%hd 0x%hhx 0x%hhx 0x%hhx",
			(short int *)&kbuf[0], &kbuf[1], &kbuf[2], &kbuf[3]);
		if (!ret) {
			pSysCmd->bCmdErr = true;
			goto out;
		}
		dev_info(pcm_dev->dev,
			"[pcmdevice]reg: dev_no=%d page=0x%02x reg=0x%02x "
			"val=0x%02x, cnt=%d\n", (int)kbuf[0], kbuf[1],
			kbuf[2], kbuf[3],
			(int)count);

		if (kbuf[0] >= pcm_dev->ndev) {
			pSysCmd->bCmdErr = true;
			pSysCmd->bufLen += snprintf(gSysCmdLog[RegSettingCmd],
				46,
				"dev_no is larger than max device num:2\n\r");
			goto out;
		}
		if (kbuf[1] > PCMDEVICE_PAGE_MAX) {
			pSysCmd->bCmdErr = true;
			pSysCmd->bufLen += snprintf(gSysCmdLog[RegSettingCmd],
				46, "Page NO is larger than %d!\n\r",
				PCMDEVICE_PAGE_MAX);
			goto out;
		}

		if (kbuf[2]&0x80) {
			pSysCmd->bCmdErr = true;
			pSysCmd->bufLen += snprintf(gSysCmdLog[RegSettingCmd],
				46, "Register NO is larger than 0x7F!\n\r");
			goto out;
		}

		n_result = pcmdevice_dev_write(pcm_dev, kbuf[0],
			PCMDEVICE_REG(kbuf[1], kbuf[2]), kbuf[3]);
		if (n_result < 0) {
			pSysCmd->bufLen += snprintf(gSysCmdLog[RegSettingCmd],
				256 - pSysCmd->bufLen,
				"%s: write P0x%02xR0x%02x failed.\n\r",
				__func__, kbuf[0], kbuf[1]);
		} else {
			pSysCmd->bCmdErr = false;
			pSysCmd->mdev = kbuf[0];
			pSysCmd->mnPage = kbuf[1];
			pSysCmd->mnReg = kbuf[2];
			gSysCmdLog[RegSettingCmd][0] = '\0';
			pSysCmd->bufLen = 0;
		}
	} else {
		pSysCmd->bCmdErr = true;
		ret = -1;
		dev_err(pcm_dev->dev, "[pcmdevice]reg: count error.\n");
	}
out:
	if (temp)
		kfree(temp);
	return count;
}

ssize_t regdump_show(struct device *dev,
	struct device_attribute *attr, char *buf)
{
	struct pcmdevice_priv *pcm_dev = dev_get_drvdata(dev);
	ssize_t len = 0;

	if (pcm_dev != NULL) {
		int i;
		const int size = PAGE_SIZE;
		int data = 0;
		int n_result = 0;
		struct Tsyscmd *pSysCmd = &pcm_dev->nSysCmd[RegDumpCmd];

		if (pSysCmd->bCmdErr == true) {
			len = scnprintf(buf, PAGE_SIZE,
				gSysCmdLog[RegDumpCmd]);
			goto out;
		}
		//15 bytes
		if (len + 20 <= size) {
			len += scnprintf(buf+len, size-len,
				"dev_no: %0d\n\r", pSysCmd->mdev);
		} else {
			scnprintf(buf + PAGE_SIZE - 100, 100,
				"%s: mem is not enough, PAGE_SIZE = %lu\n",
				__func__, PAGE_SIZE);
			len = PAGE_SIZE;
		}

		if (len + 50 <= size) {
			len += scnprintf(buf+len, size-len,
				"dev_name: %s\n\r",
				pcm_dev->dev_name);
		} else {
			scnprintf(buf + PAGE_SIZE - 100, 100,
				"%s: mem is not enough, PAGE_SIZE = %lu\n",
				__func__, PAGE_SIZE);
			len = PAGE_SIZE;
		}

		if (len + 30 <= size) {
			len += scnprintf(buf+len, size-len, "PGID: 0x%02x\n\r",
				pSysCmd->mnPage);
		} else {
			scnprintf(buf + PAGE_SIZE - 100, 100,
				"%s: mem is not enough, PAGE_SIZE = %lu\n",
				__func__, PAGE_SIZE);
			len = PAGE_SIZE;
		}
		//2560 bytes
		for (i = 0; i < 128; i++) {
			n_result = pcmdevice_dev_read(pcm_dev, pSysCmd->mdev,
				PCMDEVICE_REG(pSysCmd->mnPage, i), &data);
			if (n_result < 0) {
				len += scnprintf(buf+len, size-len,
					"%s: read register failed!\n\r",
					__func__);
				break;
			}
			//20 bytes
			if (len + 20 <= size) {
				len += scnprintf(buf+len, size-len,
					"No-%d:P0x%02xR0x%02x:0x%02x\n",
					pSysCmd->mdev, pSysCmd->mnPage,
					i, data);
			} else {
				scnprintf(buf + PAGE_SIZE - 100, 100,
					"%s: mem is not enough, "
					"PAGE_SIZE = %lu\n",
					__func__, PAGE_SIZE);
				len = PAGE_SIZE;
				break;
			}
		}
		if (len + 40 <= size)
			len += scnprintf(buf+len, size-len,
				"======caught smartpa reg end ======\n\r");
	}

out:
	return len;
}

ssize_t regdump_store(struct device *dev,
	struct device_attribute *attr, const char *buf, size_t count)
{
	struct pcmdevice_priv *pcm_dev = dev_get_drvdata(dev);
	struct Tsyscmd *pSysCmd = NULL;
	int ret = 0;
	unsigned char kbuf[2];
	char *temp = NULL;

	dev_info(pcm_dev->dev, "regdump: count = %d\n", (int)count);
	if (pcm_dev == NULL)
		return count;
	pSysCmd = &pcm_dev->nSysCmd[RegDumpCmd];
	pSysCmd->bufLen = snprintf(gSysCmdLog[RegDumpCmd],
		256, "command: echo dev 0xPG > NODE\n"
		"dev is device no, 1-digital; "
		"PG must be 2-digital HEX and less than or equeal to 4.\n"
		"eg: echo 0x00\n\r");

	if (count >= 4) {
		temp = kmalloc(count, GFP_KERNEL);
		if (!temp) {
			pSysCmd->bCmdErr = true;
			pSysCmd->bufLen += snprintf(gSysCmdLog[RegDumpCmd],
				20, "No Memory!\n\r");
			goto out;
		}
		memcpy(temp, buf, count);
		ret = sscanf(temp, "%hd 0x%hhx", (short int *)&kbuf[0],
			&kbuf[1]);
		if (!ret) {
			pSysCmd->bCmdErr = true;
			pSysCmd->bufLen += snprintf(gSysCmdLog[RegDumpCmd],
				20, "Command err!\n\r");
			goto out;
		}
		if (kbuf[0] >= pcm_dev->ndev) {
			pSysCmd->bCmdErr = true;
			pSysCmd->bufLen += snprintf(gSysCmdLog[RegSettingCmd],
				46,
				"dev_no is larger than max device num:2\n\r");
			goto out;
		}
		if (kbuf[1] > 0x4) {
			pSysCmd->bCmdErr = true;
			pSysCmd->bufLen += snprintf(gSysCmdLog[RegDumpCmd],
				64, "Page is larger than 4!\n\r");
			goto out;
		}
		pSysCmd->bCmdErr = false;
		pSysCmd->mdev = kbuf[0];
		pSysCmd->mnPage = kbuf[1];
		gSysCmdLog[RegDumpCmd][0] = '\0';
		pSysCmd->bufLen = 0;
	} else {
		pSysCmd->bCmdErr = true;
		pSysCmd->bufLen += snprintf(gSysCmdLog[RegDumpCmd],
			30, "Input params is incorrect!\n\r");
		dev_err(pcm_dev->dev, "[regdump] count error.\n");
	}
out:
	if (temp)
		kfree(temp);
	return count;
}

ssize_t regbininfo_list_show(struct device *dev,
				 struct device_attribute *attr, char *buf)
{
  	struct pcmdevice_priv *pcm_dev = dev_get_drvdata(dev);
	struct pcmdevice_regbin *regbin = &(pcm_dev->mtRegbin);
	struct pcmdevice_config_info **cfg_info = regbin->cfg_info;
	int n = 0, i = 0;

	if (pcm_dev == NULL) {
		n += scnprintf(buf+n, PAGE_SIZE - n,
			"ERROR: Can't find "
			"pcmdevice handle!\n\r");
		return n;
	}
	mutex_lock(&pcm_dev->codec_lock);
	if (n + 128 < PAGE_SIZE) {
		n += scnprintf(buf+n, PAGE_SIZE - n,
			"Regbin File Version: 0x%04X ",
			regbin->fw_hdr.binary_version_num);
		if (regbin->fw_hdr.binary_version_num < 0x105)
			n += scnprintf(buf+n, PAGE_SIZE - n,
				"No confname in this version");
		n += scnprintf(buf+n, PAGE_SIZE - n, "\n\r");
	} else {
		scnprintf(buf+PAGE_SIZE-100, 100,
			"\n[regbininfo] Out of memory!\n\r");
		n = PAGE_SIZE;
		goto out;
	}

	for (i = 0; i < regbin->ncfgs; i++) {
		if (n + 16 < PAGE_SIZE) {
			n += scnprintf(buf+n, PAGE_SIZE - n, "conf %02d", i);
		} else {
			scnprintf(buf+PAGE_SIZE-100, 100,
				"\n[regbininfo] Out of memory!\n\r");
			n = PAGE_SIZE;
			break;
		}
		if (regbin->fw_hdr.binary_version_num >= 0x105) {
			if (n + 100 < PAGE_SIZE) {
				n += scnprintf(buf+n, PAGE_SIZE-n,
					": %s\n\r", cfg_info[i]->mpName);
			} else {
				scnprintf(buf+PAGE_SIZE-100, 100,
					"\n[regbininfo] Out of memory!\n\r");
				n = PAGE_SIZE;
				break;
			}
		} else {
			n += scnprintf(buf+n, PAGE_SIZE-n, "\n\r");
		}
	}
out:
	mutex_unlock(&pcm_dev->codec_lock);
	return n;
}

ssize_t regcfg_list_store(struct device *dev,
	struct device_attribute *attr, const char *buf, size_t count)
{
	struct pcmdevice_priv *pcm_dev = dev_get_drvdata(dev);
	struct pcmdevice_regbin *regbin = NULL;
	int ret = 0;
	char *temp = NULL;
	struct Tsyscmd *pSysCmd = NULL;

	dev_info(pcm_dev->dev, "regcfg: count = %d\n", (int)count);
	if (pcm_dev == NULL)
		return count;
	mutex_lock(&pcm_dev->codec_lock);
	regbin = &(pcm_dev->mtRegbin);
	pSysCmd = &pcm_dev->nSysCmd[RegCfgListCmd];
	pSysCmd->bufLen = snprintf(gSysCmdLog[RegCfgListCmd],
		256, "command: echo CG > NODE\n"
		"CG is conf NO, it should be 2-digital decimal\n"
		"eg: echo 01 > NODE\n\r");

	if (count >= 1) {
		temp = kmalloc(count, GFP_KERNEL);
		if (!temp) {
			pSysCmd->bCmdErr = true;
			pSysCmd->bufLen += snprintf(gSysCmdLog[RegCfgListCmd],
				15, "No memory!\n");
			goto out;
		}
		memcpy(temp, buf, count);
		ret = sscanf(temp, "%hhd", &(pSysCmd->mnPage));
		if (!ret) {
			pSysCmd->bCmdErr = true;
			goto out;
		}
		dev_info(pcm_dev->dev, "[regcfg_list]cfg=%2d, cnt=%d\n",
			pSysCmd->mnPage, (int)count);
		if (pSysCmd->mnPage >= (unsigned char)regbin->ncfgs) {
			pSysCmd->bCmdErr = true;
			pSysCmd->bufLen += snprintf(gSysCmdLog[RegCfgListCmd],
			30, "Wrong conf NO!\n\r");
		} else {
			pSysCmd->bCmdErr = false;
			gSysCmdLog[RegCfgListCmd][0] = '\0';
			pSysCmd->bufLen = 0;
		}
	} else {
		pSysCmd->bCmdErr = true;
		ret = -1;
		dev_err(pcm_dev->dev, "[regcfg_list]: count error.\n");
	}
out:
	mutex_unlock(&pcm_dev->codec_lock);
	if (temp)
		kfree(temp);
	return count;
}

ssize_t regcfg_list_show(struct device *dev,
	struct device_attribute *attr, char *buf)
{
	struct pcmdevice_priv *pcm_dev = dev_get_drvdata(dev);
	ssize_t len = 0;
	int j = 0, k = 0;

	if (pcm_dev != NULL) {
		struct Tsyscmd *pSysCmd = &pcm_dev->nSysCmd[RegCfgListCmd];
		struct pcmdevice_regbin *regbin = &(pcm_dev->mtRegbin);
		struct pcmdevice_config_info **cfg_info = regbin->cfg_info;

		mutex_lock(&pcm_dev->codec_lock);
		if (pSysCmd->bCmdErr == true ||
			pSysCmd->mnPage >= regbin->ncfgs) {
			len += scnprintf(buf, pSysCmd->bufLen,
				gSysCmdLog[RegCfgListCmd]);
			goto out;
		}

		len += scnprintf(buf+len, PAGE_SIZE-len,
			"Conf %02d", pSysCmd->mnPage);
		if (regbin->fw_hdr.binary_version_num >= 0x105) {
			if (len + 100 < PAGE_SIZE) {
				len += scnprintf(buf+len, PAGE_SIZE-len,
					": %s\n\r",
					cfg_info[pSysCmd->mnPage]->mpName);
			} else {
				scnprintf(buf+PAGE_SIZE-100, 100,
					"\n[%s] Out of memory!\n\r",
					__func__);
				len = PAGE_SIZE;
				goto out;
			}
		} else {
			len += scnprintf(buf+len, PAGE_SIZE-len, "\n\r");
		}

		for (j = 0; j < (int)cfg_info[pSysCmd->mnPage]->real_nblocks;
			j++) {
			unsigned int length = 0, rc = 0;

			len += scnprintf(buf+len, PAGE_SIZE - len,
				"block type:%s\t device idx = 0x%02x\n",
				blocktype[cfg_info[pSysCmd->mnPage]->
					blk_data[j]->block_type - 1],
					cfg_info[pSysCmd->mnPage]->
					blk_data[j]->dev_idx);
			for (k = 0; k < (int)cfg_info[pSysCmd->mnPage]->
				blk_data[j]->nSublocks; k++) {
				rc = pcmdevice_process_block_show(pcm_dev,
					cfg_info[pSysCmd->mnPage]->
					blk_data[j]->regdata + length,
					cfg_info[pSysCmd->mnPage]->
					blk_data[j]->dev_idx,
					cfg_info[pSysCmd->mnPage]->
					blk_data[j]->block_size - length,
					buf, &len);
				length += rc;
				if (cfg_info[pSysCmd->mnPage]->blk_data[j]->
					block_size < length) {
					len += scnprintf(buf+len,
						PAGE_SIZE-len,
						"pcmdevice-regcfg_list: "
						"ERROR:%u %u "
						"out of memory\n", length,
						cfg_info[pSysCmd->mnPage]->
						blk_data[j]->block_size);
					break;
				}
			}
			if (length != cfg_info[pSysCmd->mnPage]->
				blk_data[j]->block_size) {
				len += scnprintf(buf+len, PAGE_SIZE-len,
					"pcmdevice-regcfg_list: ERROR: %u %u "
					"size is not same\n", length,
					cfg_info[pSysCmd->mnPage]->
					blk_data[j]->block_size);
			}
		}
out:
		mutex_unlock(&pcm_dev->codec_lock);
	} else {
		if (len + 42 < PAGE_SIZE) {
			len += scnprintf(buf + len, PAGE_SIZE - len,
				"ERROR: Can't find pcmdevice "
				"handle!\n\r");
		} else {
			scnprintf(buf+PAGE_SIZE-100, 100,
				"\n[regbininfo] Out of memory!\n\r");
			len = PAGE_SIZE;
		}
	}
	return len;
}

ssize_t fwload_store(struct device *dev,
	struct device_attribute *attr, const char *buf, size_t count)
{
	struct pcmdevice_priv *pcm_dev = dev_get_drvdata(dev);
	int ret = 0;

	dev_info(pcm_dev->dev, "fwload: count = %d\n", (int)count);
	if (pcm_dev) {
		mutex_lock(&pcm_dev->codec_lock);
		if (pcm_dev->pcm_ctrl.pcmdevice_profile_controls) {
			dev_info(pcm_dev->dev, "fw %s already loaded\n",
				pcm_dev->regbinname);
			goto out;
		}

		ret = request_firmware_nowait(THIS_MODULE,
#if KERNEL_VERSION(6, 2, 0) <= LINUX_VERSION_CODE
			FW_ACTION_UEVENT
#else
			FW_ACTION_HOTPLUG
#endif
			, pcm_dev->regbinname, pcm_dev->dev, GFP_KERNEL, pcm_dev,
			pcmdevice_regbin_ready);
		if (ret) {
			dev_err(pcm_dev->dev, "load %s error = %d\n",
				pcm_dev->regbinname, ret);
		}
out:
		mutex_unlock(&pcm_dev->codec_lock);
	}
	return count;
}

MODULE_AUTHOR("Shenghao Ding <shenghao-ding@ti.com>");
MODULE_DESCRIPTION("ASoC PCMDEVICE Driver");
MODULE_LICENSE("GPL");
