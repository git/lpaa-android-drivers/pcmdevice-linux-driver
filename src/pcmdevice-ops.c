/*
 * pcmdevice
 *
 * pcmdevice codec driver
 *
 * Copyright (C) 2020-2024 Texas Instruments Incorporated
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/module.h>
#include <sound/soc.h>

#include "pcmdevice.h"
#include "pcmdevice-ops.h"
#include "pcmdevice-rw.h"

#define PCM1690_REG_MODE_CTRL			PCMDEVICE_REG(0X0, 0x46)
#define PCM1690_REG_MODE_CTRL_DAMS_MSK		BIT(7)
#define PCM1690_REG_MODE_CTRL_DAMS_FINE_STEP	0x0
#define PCM1690_REG_MODE_CTRL_DAMS_WIDE_RANGE	0x80

int pcmdevice_info_volsw(struct snd_kcontrol *kcontrol,
	struct snd_ctl_elem_info *uinfo)
{
	struct pcmdevice_mixer_control *mc =
		(struct pcmdevice_mixer_control *)kcontrol->private_value;

	uinfo->type = SNDRV_CTL_ELEM_TYPE_INTEGER;
	uinfo->count = 1;
	uinfo->value.integer.min = 0;
	uinfo->value.integer.max = mc->max;
	return 0;
}

int pcmdevice_get_volsw(struct snd_kcontrol *kcontrol,
	struct snd_ctl_elem_value *ucontrol)
{
	struct snd_soc_component *component = snd_kcontrol_chip(kcontrol);
	struct pcmdevice_priv *pcm_dev =
		snd_soc_component_get_drvdata(component);
	struct pcmdevice_mixer_control *mc =
		(struct pcmdevice_mixer_control *)kcontrol->private_value;
	unsigned int reg = mc->reg;
	unsigned int dev_no = mc->dev_no;
	int max = mc->max;
	int rc = 0;
	unsigned int val;
	unsigned int shift = mc->shift;
	unsigned int mask = (1 << fls(max)) - 1;


	mutex_lock(&pcm_dev->codec_lock);
	rc = pcmdevice_dev_read(pcm_dev, dev_no, reg, &val);
	if (rc) {
		dev_err(pcm_dev->dev, "%s:read, ERROR, E=%d\n",
			__func__, rc);
		goto out;
	}

	val = (val >> shift) & mask;
	val = (val > max) ? max : val;
	val = mc->invert ? max - val : val;
	ucontrol->value.integer.value[0] = val;
out:
	mutex_unlock(&pcm_dev->codec_lock);
	return rc;
}

int pcmdevice_put_volsw(struct snd_kcontrol *kcontrol,
	struct snd_ctl_elem_value *ucontrol)
{
	struct snd_soc_component *component = snd_kcontrol_chip(kcontrol);
	struct pcmdevice_priv *pcm_dev =
		snd_soc_component_get_drvdata(component);
	struct pcmdevice_mixer_control *mc =
		(struct pcmdevice_mixer_control *)kcontrol->private_value;
	int err = 0;
	unsigned int reg = mc->reg;
	unsigned int dev_no = mc->dev_no;
	int max = mc->max;
	unsigned int val, val_mask;
	unsigned int shift = mc->shift;
	unsigned int mask = (1 << fls(max)) - 1;

	mutex_lock(&pcm_dev->codec_lock);
	val = ucontrol->value.integer.value[0] & mask;
	val = (val > max) ? max : val;
	val = mc->invert ? max - val : val;
	val_mask = mask << shift;
	val = val << shift;
	err = pcmdevice_dev_update_bits(pcm_dev, dev_no, reg, val_mask,
		val);
	if (err) {
		dev_err(pcm_dev->dev, "%s:update_bits, ERROR, E=%d\n",
			__func__, err);
		goto out;
	}
out:
	mutex_unlock(&pcm_dev->codec_lock);
	return err;
}

int pcm1690_get_volsw(struct snd_kcontrol *kcontrol,
	struct snd_ctl_elem_value *ucontrol)
{
	struct snd_soc_component *component = snd_kcontrol_chip(kcontrol);
	struct pcmdevice_priv *pcm_dev =
		snd_soc_component_get_drvdata(component);
	struct pcmdevice_mixer_control *mc =
		(struct pcmdevice_mixer_control *)kcontrol->private_value;
	unsigned int reg = mc->reg;
	unsigned int dev_no = mc->dev_no;
	int max = mc->max;
	int rc = 0;
	unsigned int val;
	unsigned int shift = mc->shift;
	unsigned int mask = (1 << fls(max)) - 1;

	mutex_lock(&pcm_dev->codec_lock);
	rc = pcmdevice_dev_read(pcm_dev, dev_no, PCM1690_REG_MODE_CTRL,
		&val);
	if (rc) {
		dev_err(pcm_dev->dev, "%s:read mode, ERROR, E=%d\n",
			__func__, rc);
		goto out;
	}
	if (!(val & PCM1690_REG_MODE_CTRL_DAMS_MSK)) {
		dev_info(pcm_dev->dev,
			"%s: Pls set to wide-range mode, before using this control\n",
			__func__);
		ucontrol->value.integer.value[0] = -25500;
		goto out;
	}
	rc = pcmdevice_dev_read(pcm_dev, dev_no, reg, &val);
	if (rc) {
		dev_err(pcm_dev->dev, "%s:read, ERROR, E=%d\n",
			__func__, rc);
		goto out;
	}

	val = (val >> shift) & mask;
	val = (val > max) ? max : val;
	val = mc->invert ? max - val : val;
	ucontrol->value.integer.value[0] = val;
out:
	mutex_unlock(&pcm_dev->codec_lock);
	return rc;
}

int pcm1690_put_volsw(struct snd_kcontrol *kcontrol,
	struct snd_ctl_elem_value *ucontrol)
{
	struct snd_soc_component *component = snd_kcontrol_chip(kcontrol);
	struct pcmdevice_priv *pcm_dev =
		snd_soc_component_get_drvdata(component);
	struct pcmdevice_mixer_control *mc =
		(struct pcmdevice_mixer_control *)kcontrol->private_value;
	int err = 0;
	unsigned int reg = mc->reg;
	unsigned int dev_no = mc->dev_no;
	int max = mc->max;
	unsigned int val, val_mask;
	unsigned int shift = mc->shift;
	unsigned int mask = (1 << fls(max)) - 1;

	mutex_lock(&pcm_dev->codec_lock);
	err = pcmdevice_dev_read(pcm_dev, dev_no, PCM1690_REG_MODE_CTRL,
		&val);
	if (err) {
		dev_err(pcm_dev->dev, "%s:read mode, ERROR, E=%d\n",
			__func__, err);
		goto out;
	}
	if (!(val & PCM1690_REG_MODE_CTRL_DAMS_MSK)) {
		dev_info(pcm_dev->dev,
			"%s: Pls set to wide-range mode, before using this control\n",
			__func__);
		goto out;
	}
	val = ucontrol->value.integer.value[0] & mask;
	val = (val > max) ? max : val;
	val = mc->invert ? max - val : val;
	val_mask = mask << shift;
	val = val << shift;
	err = pcmdevice_dev_update_bits(pcm_dev, dev_no, reg, val_mask,
		val);
	if (err) {
		dev_err(pcm_dev->dev, "%s:update_bits, ERROR, E=%d\n",
			__func__, err);
		goto out;
	}
out:
	mutex_unlock(&pcm_dev->codec_lock);
	return err;
}

int pcm1690_get_finevolsw(struct snd_kcontrol *kcontrol,
		struct snd_ctl_elem_value *ucontrol)
{
	struct snd_soc_component *component = snd_kcontrol_chip(kcontrol);
	struct pcmdevice_priv *pcm_dev =
		snd_soc_component_get_drvdata(component);
	struct pcmdevice_mixer_control *mc =
		(struct pcmdevice_mixer_control *)kcontrol->private_value;
	unsigned int reg = mc->reg;
	unsigned int dev_no = mc->dev_no;
	int max = mc->max;
	int rc = 0;
	unsigned int val;
	unsigned int shift = mc->shift;
	unsigned int mask = (1 << fls(max)) - 1;

	mutex_lock(&pcm_dev->codec_lock);
	rc = pcmdevice_dev_read(pcm_dev, dev_no, PCM1690_REG_MODE_CTRL,
		&val);
	if (rc) {
		dev_err(pcm_dev->dev, "%s:read mode, ERROR, E=%d\n",
			__func__, rc);
		goto out;
	}
	if (val & PCM1690_REG_MODE_CTRL_DAMS_MSK) {
		dev_info(pcm_dev->dev,
			"%s: Pls set to fine mode, before using this control\n",
			__func__);
		ucontrol->value.integer.value[0] = -12750;
		goto out;
	}
	rc = pcmdevice_dev_read(pcm_dev, dev_no, reg, &val);
	if (rc) {
		dev_err(pcm_dev->dev, "%s:read, ERROR, E=%d\n",
			__func__, rc);
		goto out;
	}

	val = (val >> shift) & mask;
	val = (val > max) ? max : val;
	val = mc->invert ? max - val : val;
	ucontrol->value.integer.value[0] = val;
out:
	mutex_unlock(&pcm_dev->codec_lock);
	return rc;
}

int pcm1690_put_finevolsw(struct snd_kcontrol *kcontrol,
	struct snd_ctl_elem_value *ucontrol)
{
	struct snd_soc_component *component = snd_kcontrol_chip(kcontrol);
	struct pcmdevice_priv *pcm_dev =
		snd_soc_component_get_drvdata(component);
	struct pcmdevice_mixer_control *mc =
		(struct pcmdevice_mixer_control *)kcontrol->private_value;
	int err = 0;
	unsigned int reg = mc->reg;
	unsigned int dev_no = mc->dev_no;
	int max = mc->max;
	unsigned int val, val_mask;
	unsigned int shift = mc->shift;
	unsigned int mask = (1 << fls(max)) - 1;

	mutex_lock(&pcm_dev->codec_lock);
	err = pcmdevice_dev_read(pcm_dev, dev_no, PCM1690_REG_MODE_CTRL,
		&val);
	if (err) {
		dev_err(pcm_dev->dev, "%s:read mode, ERROR, E=%d\n",
			__func__, err);
		goto out;
	}

	if (val & PCM1690_REG_MODE_CTRL_DAMS_MSK) {
		dev_info(pcm_dev->dev,
			"%s: Pls set to fine-mode, before using this control\n",
			__func__);
		goto out;
	}
	val = ucontrol->value.integer.value[0] & mask;
	val = (val > max) ? max : val;
	val = mc->invert ? max - val : val;
	val_mask = mask << shift;
	val = val << shift;
	err = pcmdevice_dev_update_bits(pcm_dev, dev_no, reg, val_mask,
		val);
	if (err) {
		dev_err(pcm_dev->dev, "%s:update_bits, ERROR, E=%d\n",
			__func__, err);
		goto out;
	}
out:
	mutex_unlock(&pcm_dev->codec_lock);
	return err;
}

MODULE_AUTHOR("Shenghao Ding <shenghao-ding@ti.com>");
MODULE_DESCRIPTION("ASoC PCMDEVICE Driver");
MODULE_LICENSE("GPL");
