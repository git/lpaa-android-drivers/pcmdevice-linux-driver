/*
 * pcmdevice
 *
 * pcmdevice codec driver
 *
 * Copyright (C) 2020-2024 Texas Instruments Incorporated
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __PCM6260_REG_H__
#define __PCM6260_REG_H__

#define PCM6260_REG_INT_LTCH0	PCMDEVICE_REG(0X0, 0x2c)
#define PCM6260_REG_CHX_LTCH	PCMDEVICE_REG(0X0, 0x2d)
#define PCM6260_REG_CH1_LTCH	PCMDEVICE_REG(0X0, 0x2e)
#define PCM6260_REG_CH2_LTCH	PCMDEVICE_REG(0X0, 0x2f)
#define PCM6260_REG_CH3_LTCH	PCMDEVICE_REG(0X0, 0x30)
#define PCM6260_REG_CH4_LTCH	PCMDEVICE_REG(0X0, 0x31)
#define PCM6260_REG_CH5_LTCH	PCMDEVICE_REG(0X0, 0x32)
#define PCM6260_REG_CH6_LTCH	PCMDEVICE_REG(0X0, 0x33)
#define PCM6260_REG_INT_LTCH1	PCMDEVICE_REG(0X0, 0x35)
#define PCM6260_REG_INT_LTCH2	PCMDEVICE_REG(0X0, 0x36)
#define PCM6260_REG_INT_LTCH3	PCMDEVICE_REG(0X0, 0x37)

#endif
