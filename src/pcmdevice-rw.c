/*
 * pcmdevice
 *
 * pcmdevice codec driver
 *
 * Copyright (C) 2020-2024 Texas Instruments Incorporated
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/firmware.h>
#include <linux/i2c.h>
#include <linux/module.h>
#include <linux/regmap.h>

#include "pcmdevice.h"
#include "pcmdevice-rw.h"

static int pcmdevice_regmap_write(
	struct pcmdevice_priv *pcm_dev,
	unsigned int reg, unsigned int value)
{
	int nResult = 0;
	int retry_count = PCMDEVICE_I2C_RETRY_COUNT;

	while (retry_count--) {
		nResult = regmap_write(pcm_dev->regmap, reg,
			value);
		if (nResult >= 0)
			break;
		usleep_range(5000, 5050);
	}
	if (retry_count == -1)
		return PCMDEVICE_ERROR_I2C_FAILED;
	else
		return 0;
}

static int pcmdevice_regmap_bulk_write(
	struct pcmdevice_priv *pcm_dev, unsigned int reg,
	unsigned char *pData, unsigned int nLength)
{
	int nResult = 0;
	int retry_count = PCMDEVICE_I2C_RETRY_COUNT;

	while (retry_count--) {
		nResult = regmap_bulk_write(pcm_dev->regmap, reg,
				pData, nLength);
		if (nResult >= 0)
			break;
		usleep_range(5000, 5050);
	}
	if (retry_count == -1)
		return PCMDEVICE_ERROR_I2C_FAILED;
	else
		return 0;
}

static int pcmdevice_regmap_read(
	struct pcmdevice_priv *pcm_dev,
	unsigned int reg, unsigned int *value)
{
	int nResult = 0;
	int retry_count = PCMDEVICE_I2C_RETRY_COUNT;

	while (retry_count--) {
		nResult = regmap_read(pcm_dev->regmap, reg,
			value);
		if (nResult >= 0)
			break;
		usleep_range(5000, 5050);
	}
	if (retry_count == -1)
		return PCMDEVICE_ERROR_I2C_FAILED;
	else
		return 0;
}

static int pcmdevice_regmap_bulk_read(
	struct pcmdevice_priv *pcm_dev, unsigned int reg,
	unsigned char *pData, unsigned int nLength)
{
	int nResult = 0;
	int retry_count = PCMDEVICE_I2C_RETRY_COUNT;

	while (retry_count--) {
		nResult = regmap_bulk_read(pcm_dev->regmap, reg,
			pData, nLength);
		if (nResult >= 0)
			break;
		usleep_range(5000, 5050);
	}
	if (retry_count == -1)
		return PCMDEVICE_ERROR_I2C_FAILED;
	else
		return 0;
}

static int pcmdevice_regmap_update_bits(
	struct pcmdevice_priv *pcm_dev, unsigned int reg,
	unsigned int mask, unsigned int value)
{
	int nResult = 0;
	int retry_count = PCMDEVICE_I2C_RETRY_COUNT;

	while (retry_count--) {
		nResult = regmap_update_bits(pcm_dev->regmap, reg,
			mask, value);
		if (nResult >= 0)
			break;
		usleep_range(5000, 5050);
	}
	if (retry_count == -1)
		return PCMDEVICE_ERROR_I2C_FAILED;
	else
		return 0;
}

int pcmdevice_dev_read(struct pcmdevice_priv *pcm_dev,
	unsigned int dev_no, unsigned int reg, unsigned int *pValue)
{
	int ret = 0;

	mutex_lock(&pcm_dev->dev_lock);
	if (dev_no < pcm_dev->ndev) {
		struct pcmdevice *p = &pcm_dev->pcmdevice[dev_no];

		if (pcm_dev->client->addr != p->addr) {
			pcm_dev->client->addr = p->addr;
			ret = pcmdevice_regmap_write(pcm_dev,
					PCMDEVICE_PAGE_SELECT, 0);
			if (ret < 0) {
				dev_err(pcm_dev->dev, "%s, E=%d\n",
					__func__, ret);
				goto out;
			}
		}

		ret = pcmdevice_regmap_read(pcm_dev, reg, pValue);
		if (ret < 0)
			dev_err(pcm_dev->dev, "read, ERROR, E=%d\n",
				ret);
		else
			dev_dbg(pcm_dev->dev,
				"read PAGE:REG 0x%02x:0x%02x,0x%02x\n",
				PCMDEVICE_PAGE_ID(reg),
				PCMDEVICE_PAGE_REG(reg), *pValue);
	} else
		dev_err(pcm_dev->dev, "%s, ERROR: no such device(%d)\n",
			__func__, dev_no);

out:
	mutex_unlock(&pcm_dev->dev_lock);
	return ret;
}

int pcmdevice_dev_write(struct pcmdevice_priv *pcm_dev,
	unsigned int dev_no, unsigned int reg, unsigned int value)
{
	int ret = 0;

	mutex_lock(&pcm_dev->dev_lock);
	if (dev_no < pcm_dev->ndev) {
		struct pcmdevice *p = &pcm_dev->pcmdevice[dev_no];

		if (pcm_dev->client->addr != p->addr) {
			pcm_dev->client->addr = p->addr;
			ret = pcmdevice_regmap_write(pcm_dev,
					PCMDEVICE_PAGE_SELECT, 0);
			if (ret < 0) {
				dev_err(pcm_dev->dev, "%s, E=%d\n",
					__func__, ret);
				goto out;
			}
		}

		ret = pcmdevice_regmap_write(pcm_dev, reg, value);
		if (ret < 0)
			dev_err(pcm_dev->dev, "write ERROR E=%d\n", ret);
		else
			dev_dbg(pcm_dev->dev,
				"write PAGE:REG 0x%02x:0x%02x, VAL: 0x%02x\n",
				PCMDEVICE_PAGE_ID(reg),
				PCMDEVICE_PAGE_REG(reg), value);
	} else
		dev_err(pcm_dev->dev, "%s, ERROR: no such device(%d)\n",
			__func__, dev_no);

out:
	mutex_unlock(&pcm_dev->dev_lock);
	return ret;
}

int pcmdevice_dev_bulk_write(struct pcmdevice_priv *pcm_dev,
	unsigned int dev_no, unsigned int reg, unsigned char *p_data,
	unsigned int n_length)
{
	int ret = 0;

	mutex_lock(&pcm_dev->dev_lock);
	if (dev_no < pcm_dev->ndev) {
		struct pcmdevice *p = &pcm_dev->pcmdevice[dev_no];

		if (pcm_dev->client->addr != p->addr) {
			pcm_dev->client->addr = p->addr;
			ret = pcmdevice_regmap_write(pcm_dev,
					PCMDEVICE_PAGE_SELECT, 0);
			if (ret < 0) {
				dev_err(pcm_dev->dev, "%s, E=%d\n",
					__func__, ret);
				goto out;
			}
		}

		ret = pcmdevice_regmap_bulk_write(pcm_dev,
			reg, p_data, n_length);
		if (ret < 0)
			dev_err(pcm_dev->dev, "bulk_write ERROR, E=%d\n",
				ret);
		else
			dev_dbg(pcm_dev->dev,
				"bulk_write PAGE:REG 0x%02x:0x%02x, "
				"len: 0x%02x\n",
				PCMDEVICE_PAGE_ID(reg),
				PCMDEVICE_PAGE_REG(reg), n_length);
	} else
		dev_err(pcm_dev->dev, "%s, ERROR: no such channel(%d)\n",
			__func__, dev_no);

out:
	mutex_unlock(&pcm_dev->dev_lock);
	return ret;
}

int pcmdevice_dev_bulk_read(struct pcmdevice_priv *pcm_dev,
	unsigned int dev_no, unsigned int reg, unsigned char *p_data,
	unsigned int n_length)
{
	int ret = 0;

	mutex_lock(&pcm_dev->dev_lock);
	if (dev_no < pcm_dev->ndev) {
		struct pcmdevice *p = &pcm_dev->pcmdevice[dev_no];

		if (pcm_dev->client->addr != p->addr) {
			pcm_dev->client->addr = p->addr;
			ret = pcmdevice_regmap_write(pcm_dev,
					PCMDEVICE_PAGE_SELECT, 0);
			if (ret < 0) {
				dev_err(pcm_dev->dev, "%s, E=%d\n",
					__func__, ret);
				goto out;
			}
		}

		ret = pcmdevice_regmap_bulk_read(pcm_dev,
			reg, p_data, n_length);
		if (ret < 0)
			dev_err(pcm_dev->dev, "bulk_read ERROR E=%d\n",
				ret);
		else
			dev_dbg(pcm_dev->dev,
				"bulk_read PAGE:REG 0x%02x:0x%02x, "
				"len: 0x%02x\n", PCMDEVICE_PAGE_ID(reg),
				PCMDEVICE_PAGE_REG(reg), n_length);
	} else
		dev_err(pcm_dev->dev, "%s, ERROR: no such channel(%d)\n",
			__func__, dev_no);

out:
	mutex_unlock(&pcm_dev->dev_lock);
	return ret;
}

int pcmdevice_dev_update_bits(
	struct pcmdevice_priv *pcm_dev, unsigned int dev_no, unsigned int reg,
	unsigned int mask, unsigned int value)
{
	int ret = 0;

	mutex_lock(&pcm_dev->dev_lock);
	if (dev_no < pcm_dev->ndev) {
		struct pcmdevice *p = &pcm_dev->pcmdevice[dev_no];

		if (pcm_dev->client->addr != p->addr) {
			pcm_dev->client->addr = p->addr;
			ret = pcmdevice_regmap_write(pcm_dev,
					PCMDEVICE_PAGE_SELECT, 0);
			if (ret < 0) {
				dev_err(pcm_dev->dev, "%s, E=%d\n",
					__func__, ret);
				goto out;
			}
		}

		ret = pcmdevice_regmap_update_bits(pcm_dev,
			reg, mask, value);
		if (ret < 0)
			dev_err(pcm_dev->dev, "update_bits ERROR, E=%d\n",
				ret);
		else
			dev_dbg(pcm_dev->dev,
				"update_bits PAGE:REG 0x%02x:0x%02x, "
				"mask: 0x%02x, val: 0x%02x\n",
				PCMDEVICE_PAGE_ID(reg),
				PCMDEVICE_PAGE_REG(reg), mask, value);
	} else
		dev_err(pcm_dev->dev, "%s, ERROR: no such channel(%d)\n",
			__func__, dev_no);

out:
	mutex_unlock(&pcm_dev->dev_lock);
	return ret;
}

MODULE_AUTHOR("Shenghao Ding <shenghao-ding@ti.com>");
MODULE_DESCRIPTION("ASoC PCMDEVICE Driver");
MODULE_LICENSE("GPL");
