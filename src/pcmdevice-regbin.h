/*
 * pcmdevice
 *
 * pcmdevice codec driver
 *
 * Copyright (C) 2020-2024 Texas Instruments Incorporated
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __PCMDEVICE_REGBIN_H__
#define __PCMDEVICE_REGBIN_H__

#define PCMDEVICE_CONFIG_SUM  (64)
#define PCMDEVICE_DEVICE_SUM  (8)

#define PCMDEVICE_CMD_SING_W  (0x1)
#define PCMDEVICE_CMD_BURST  (0x2)
#define PCMDEVICE_CMD_DELAY  (0x3)
#define PCMDEVICE_CMD_FIELD_W  (0x4)

enum pcmdevice_bin_blk_type {
	PCMDEVICE_BIN_BLK_COEFF = 1,
	PCMDEVICE_BIN_BLK_POST_POWER_UP,
	PCMDEVICE_BIN_BLK_PRE_SHUTDOWN,
	PCMDEVICE_BIN_BLK_PRE_POWER_UP,
	PCMDEVICE_BIN_BLK_POST_SHUTDOWN
};

struct pcmdevice_regbin_hdr {
	unsigned int img_sz;
	unsigned int checksum;
	unsigned int binary_version_num;
	unsigned int drv_fw_version;
	unsigned int timestamp;
	unsigned char plat_type;
	unsigned char dev_family;
	unsigned char reserve;
	unsigned char ndev;
	unsigned char devs[PCMDEVICE_DEVICE_SUM];
	unsigned int nconfig;
	unsigned int config_size[PCMDEVICE_CONFIG_SUM];
};

struct pcmdevice_block_data {
	unsigned char dev_idx;
	unsigned char block_type;
	unsigned short yram_checksum;
	unsigned int block_size;
	unsigned int nSublocks;
	unsigned char *regdata;
};

struct pcmdevice_config_info {
	char mpName[64];
	unsigned int nblocks;
	unsigned int real_nblocks;
	unsigned char active_dev;
	struct pcmdevice_block_data **blk_data;
};

struct pcmdevice_regbin {
	struct pcmdevice_regbin_hdr fw_hdr;
	int ncfgs;
	struct pcmdevice_config_info **cfg_info;
};

void pcmdevice_regbin_ready(const struct firmware *pFW,
	void *pContext);
void pcmdevice_config_info_remove(void *pContext);
void pcmdevice_select_cfg_blk(void *pContext, int conf_no,
	unsigned char block_type);
int pcmdevice_process_block(void *pContext,
	unsigned char *data, unsigned char dev_idx, int sublocksize);
int pcmdevice_process_block_show(void *pContext,
	unsigned char *data, unsigned char dev_idx,
	int sublocksize, char *buf, ssize_t *len);
#endif
