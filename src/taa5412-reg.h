/*
 * pcmdevice
 *
 * pcmdevice codec driver
 *
 * Copyright (C) 2020-2024 Texas Instruments Incorporated
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __TAA5412_REG_H__
#define __TAA5412_REG_H__

#define TAA5412_REG_INT_LTCH0	PCMDEVICE_REG(0X0, 0x34)
#define TAA5412_REG_CHX_LTCH	PCMDEVICE_REG(0X0, 0x35)
#define TAA5412_REG_IN_CH1_LTCH	PCMDEVICE_REG(0X0, 0x36)
#define TAA5412_REG_IN_CH2_LTCH	PCMDEVICE_REG(0X0, 0x37)
#define TAA5412_REG_INT_LTCH1	PCMDEVICE_REG(0X0, 0x3a)
#define TAA5412_REG_INT_LTCH2	PCMDEVICE_REG(0X0, 0x3b)

#endif
