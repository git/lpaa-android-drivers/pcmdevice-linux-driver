/*
 * pcmdevice
 *
 * pcmdevice codec driver
 *
 * Copyright (C) 2020-2024 Texas Instruments Incorporated
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __PCMDEVICE_OPS_H__
#define __PCMDEVICE_OPS_H__

/* mixer control */
struct pcmdevice_mixer_control {
	int max;
	int reg;
	unsigned int dev_no;
	unsigned int shift;
	unsigned int invert;
};
struct pcmdev_ctrl_info {
	const unsigned int *gain;
	struct pcmdevice_mixer_control *pcmdev_ctrl;
	unsigned int ctrl_array_size;
};
int pcmdevice_info_volsw(struct snd_kcontrol *kcontrol,
	struct snd_ctl_elem_info *uinfo);

int pcmdevice_get_volsw(struct snd_kcontrol *kcontrol,
	struct snd_ctl_elem_value *ucontrol);

int pcmdevice_put_volsw(struct snd_kcontrol *kcontrol,
	struct snd_ctl_elem_value *ucontrol);

int pcm1690_get_volsw(struct snd_kcontrol *kcontrol,
	struct snd_ctl_elem_value *ucontrol);

int pcm1690_get_finevolsw(struct snd_kcontrol *kcontrol,
	struct snd_ctl_elem_value *ucontrol);

int pcm1690_put_volsw(struct snd_kcontrol *kcontrol,
	struct snd_ctl_elem_value *ucontrol);

int pcm1690_put_finevolsw(struct snd_kcontrol *kcontrol,
	struct snd_ctl_elem_value *ucontrol);

#endif
