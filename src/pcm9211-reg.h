/*
 * pcmdevice
 *
 * pcmdevice codec driver
 *
 * Copyright (C) 2020-2024 Texas Instruments Incorporated
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __PCM9211_REG_H__
#define __PCM9211_REG_H__

#define PCM9211_REG_INT0_OUTPUT	PCMDEVICE_REG(0X0, 0x2c)
#define PCM9211_REG_INT1_OUTPUT	PCMDEVICE_REG(0X0, 0x2d)
#define PCM9211_REG_SW_CTRL		PCMDEVICE_REG(0X0, 0x40)
#define PCM9211_REG_SW_CTRL_MRST_MSK	BIT(7)
#define PCM9211_REG_SW_CTRL_MRST	0x0

#endif
