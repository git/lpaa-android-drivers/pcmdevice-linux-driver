/*
 * pcmdevice
 *
 * pcmdevice codec driver
 *
 * Copyright (C) 2020-2024 Texas Instruments Incorporated
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <asm/unaligned.h>
#include <linux/firmware.h>
#include <linux/gpio/consumer.h>
#include <linux/i2c.h>
#include <linux/module.h>
#include <linux/regmap.h>
#include <linux/slab.h>

#include "pcmdevice.h"
#include "pcmdevice-rw.h"

const char *blocktype[5] = {
	"COEFF",
	"POST_POWER_UP",
	"PRE_SHUTDOWN",
	"PRE_POWER_UP",
	"POST_SHUTDOWN"
};

int pcmdevice_process_block(void *pContext,
	unsigned char *data, unsigned char dev_idx, int sublocksize)
{
	struct pcmdevice_priv *pcm_dev =
		(struct pcmdevice_priv *)pContext;
	unsigned char subblk_typ = data[1];
	int subblk_offset = 2;
	int chn = 0, chnend = 0;
	int rc = 0;

	if (dev_idx) {
		chn = dev_idx - 1;
		chnend = dev_idx;
	} else {
		chn = 0;
		chnend = pcm_dev->ndev;
	}

	for (; chn < chnend; chn++) {
		if (pcm_dev->pcmdevice[chn].is_loading == false)
			goto out;

		subblk_offset = 2;
		switch (subblk_typ) {
		case PCMDEVICE_CMD_SING_W: {
			int i = 0;
			unsigned short len = get_unaligned_be16(&data[2]);

			subblk_offset += 2;
			if (subblk_offset + 4 * len > sublocksize) {
				dev_err(pcm_dev->dev,
					"process_block: Out of memory\n");
				break;
			}

			for (i = 0; i < len; i++) {
					rc = pcmdevice_dev_write(pcm_dev, chn,
					PCMDEVICE_REG(data[subblk_offset + 1],
						data[subblk_offset + 2]),
					data[subblk_offset + 3]);
				if (rc < 0)
					dev_err(pcm_dev->dev,
							"process_block: single write "
							"error\n");

				subblk_offset += 4;
			}
		}
		break;
		case PCMDEVICE_CMD_BURST: {
			unsigned short len = get_unaligned_be16(&data[2]);

			subblk_offset += 2;
			if (subblk_offset + 4 + len > sublocksize) {
				dev_err(pcm_dev->dev,
					"process_block: BURST Out of memory\n");
				break;
			}
			if (len % 4) {
				dev_err(pcm_dev->dev,
					"process_block: Burst len(%u) "
					"can be divided by 4\n", len);
				break;
			}

				rc = pcmdevice_dev_bulk_write(pcm_dev, chn,
				PCMDEVICE_REG(data[subblk_offset + 1],
					data[subblk_offset + 2]),
				&(data[subblk_offset + 4]), len);
			if (rc < 0)
				dev_err(pcm_dev->dev,
					"process_block: bulk_write error = %d\n", rc);

			subblk_offset += (len + 4);
		}
			break;
		case PCMDEVICE_CMD_DELAY: {
			unsigned short delay_time = 0;

			if (subblk_offset + 2 > sublocksize) {
				dev_err(pcm_dev->dev,
					"process_block: delay Out of memory\n");
				break;
			}
			delay_time = get_unaligned_be16(&data[2]);
			usleep_range(delay_time*1000, delay_time*1000);
			subblk_offset += 2;
		}
			break;
		case PCMDEVICE_CMD_FIELD_W:
		if (subblk_offset + 6 > sublocksize) {
			dev_err(pcm_dev->dev,
				"process_block: bit write Out of memory\n");
			break;
		}
			rc = pcmdevice_dev_update_bits(pcm_dev, chn,
				PCMDEVICE_REG(data[subblk_offset + 3],
				data[subblk_offset + 4]), data[subblk_offset + 1],
				data[subblk_offset + 5]);
		if (rc < 0)
			dev_err(pcm_dev->dev,
				"process_block: update_bits error = %d\n", rc);

		subblk_offset += 6;
			break;
		default:
			break;
		}
	}
out:
	return subblk_offset;
}

int pcmdevice_process_block_show(void *pContext,
	unsigned char *data, unsigned char dev_idx,
	int sublocksize, char *buf, ssize_t *length)
{
	struct pcmdevice_priv *pcm_dev =
		(struct pcmdevice_priv *)pContext;
	unsigned char subblk_typ = data[1];
	int subblk_offset = 2;
	int chn = 0, chnend = 0;

	if (dev_idx) {
		chn = dev_idx - 1;
		chnend = dev_idx;
	} else {
		chn = 0;
		chnend = pcm_dev->ndev;
	}
	for (; chn < chnend; chn++) {
		subblk_offset = 2;
		switch (subblk_typ) {
		case PCMDEVICE_CMD_SING_W: {
			int i = 0;
			unsigned short len = get_unaligned_be16(&data[2]);

			subblk_offset += 2;
				*length += scnprintf(buf + *length,
					PAGE_SIZE - *length,
				"\t\tSINGLE BYTE:\n");
			if (subblk_offset + 4 * len > sublocksize) {
				*length += scnprintf(buf + *length,
					PAGE_SIZE - *length,
					"CMD_SING_W: Out of memory\n");
				break;
			}

			for (i = 0; i < len; i++) {
				*length += scnprintf(buf + *length,
					PAGE_SIZE - *length,
						"\t\t\tPAGE0x%02x REG0x%02x VALUE = "
						"0x%02x\n",
					data[subblk_offset + 1],
					data[subblk_offset + 2],
					data[subblk_offset + 3]);
				subblk_offset += 4;
			}
		}
			break;
		case PCMDEVICE_CMD_BURST: {
			int i = 0;
			unsigned short len = get_unaligned_be16(&data[2]);
			unsigned char reg = 0;

			subblk_offset += 2;
			*length += scnprintf(buf + *length,
				PAGE_SIZE - *length, "\t\tBURST:\n");
			if (subblk_offset + 4 + len > sublocksize) {
				*length += scnprintf(buf + *length,
					PAGE_SIZE - *length,
					"CMD_BURST: Out of memory.\n");
				break;
			}
			if (len % 4) {
				*length += scnprintf(buf + *length,
					PAGE_SIZE - *length,
					"CMD_BURST: Burst len is wrong\n");
				break;
			}
			reg = data[subblk_offset + 2];
				*length += scnprintf(buf + *length,
					PAGE_SIZE - *length,
				"\t\t\tPAGE0x%02x\n", data[subblk_offset + 1]);
			subblk_offset += 4;
			for (i = 0; i < len / 4; i++) {
				*length += scnprintf(buf + *length,
					PAGE_SIZE - *length,
					"\t\t\tREG0x%02x = 0x%02x "
					"REG0x%02x = 0x%02x REG0x%02x = 0x%02x"
					" REG0x%02x = 0x%02x\n",
					reg + i*4, data[subblk_offset + 0],
					reg + i*4+1, data[subblk_offset + 1],
					reg + i * 4 + 2,
					data[subblk_offset + 2],
					reg + i * 4 + 3,
					data[subblk_offset + 3]);
				subblk_offset += 4;
			}
		}
			break;
		case PCMDEVICE_CMD_DELAY: {
			unsigned short delay_time = 0;

			if (subblk_offset + 2 > sublocksize) {
				*length += scnprintf(buf + *length,
					PAGE_SIZE - *length,
					"CMD_DELAY: Out of memory\n");
				break;
			}
			delay_time = get_unaligned_be16(&data[2]);
			*length += scnprintf(buf + *length,
				PAGE_SIZE - *length,
				"\t\tDELAY = %ums\n", delay_time);
			subblk_offset += 2;
		}
			break;
		case PCMDEVICE_CMD_FIELD_W:
			if (subblk_offset + 6 > sublocksize) {
				*length += scnprintf(buf + *length,
					PAGE_SIZE - *length,
					"FIELD_W: Out of memory\n");
				break;
			}
			*length += scnprintf(buf + *length, PAGE_SIZE -
				*length, "\t\tFIELD:\n");
			*length += scnprintf(buf + *length, PAGE_SIZE -
				*length,
				"\t\t\tPAGE0x%02x REG0x%02x MASK0x%02x "
				"VALUE = 0x%02x\n", data[subblk_offset + 3],
				data[subblk_offset + 4],
				data[subblk_offset + 1],
				data[subblk_offset + 5]);

			subblk_offset += 6;
			break;
		default:
			break;
		};
	}
	return subblk_offset;
}

void pcmdevice_select_cfg_blk(void *pContext, int conf_no,
	unsigned char block_type)
{
	struct pcmdevice_priv *pcm_dev =
		(struct pcmdevice_priv *) pContext;
	struct pcmdevice_regbin *regbin = &(pcm_dev->mtRegbin);
	struct pcmdevice_config_info **cfg_info = regbin->cfg_info;
	int j = 0, k = 0, chn = 0, chnend = 0;

	if (conf_no >= regbin->ncfgs || conf_no < 0 || NULL == cfg_info) {
		dev_err(pcm_dev->dev,
			"conf_no should be less than %u\n",
			regbin->ncfgs);
		goto out;
	} else {
		dev_info(pcm_dev->dev,
			"select_cfg_blk: profile_conf_id = %d\n",
			conf_no);
	}
	for (j = 0; j < (int)cfg_info[conf_no]->real_nblocks; j++) {
		unsigned int length = 0, rc = 0;

		if (block_type > 5 || block_type < 2) {
			dev_err(pcm_dev->dev,
				"ERROR!!!block_type should be in range "
				"from 2 to 5\n");
			goto out;
		}
		if (block_type != cfg_info[conf_no]->blk_data[j]->block_type)
			continue;
		dev_info(pcm_dev->dev, "select_cfg_blk: conf %d, "
			"block type:%s\t device idx = 0x%02x\n",
			conf_no, blocktype[cfg_info[conf_no]->blk_data[j]
			->block_type - 1], cfg_info[conf_no]->blk_data[j]
			->dev_idx);

		for (k = 0; k < (int)cfg_info[conf_no]->blk_data[j]
			->nSublocks; k++) {
			if (cfg_info[conf_no]->blk_data[j]->dev_idx) {
				chn = cfg_info[conf_no]->blk_data[j]->dev_idx
					- 1;
				chnend = cfg_info[conf_no]->blk_data[j]->
					dev_idx;
			} else {
				chn = 0;
				chnend = pcm_dev->ndev;
			}
			for (; chn < chnend; chn++)
				pcm_dev->pcmdevice[chn].is_loading = true;
			rc = pcmdevice_process_block(pcm_dev,
				cfg_info[conf_no]->blk_data[j]->regdata +
				length,
				cfg_info[conf_no]->blk_data[j]->dev_idx,
				cfg_info[conf_no]->blk_data[j]->block_size -
				length);
			length += rc;
			if (cfg_info[conf_no]->blk_data[j]->block_size <
				length) {
				dev_err(pcm_dev->dev, "select_cfg_blk: "
					"ERROR:%u %u out of memory\n", length,
					cfg_info[conf_no]->blk_data[j]->
					block_size);
				break;
			}
		}
		if (length != cfg_info[conf_no]->blk_data[j]->block_size) {
			dev_err(pcm_dev->dev,
				"select_cfg_blk: ERROR: %u %u size is not "
				"same\n", length,
				cfg_info[conf_no]->blk_data[j]->block_size);
		}
	}

out:
	return;
}

static struct pcmdevice_config_info *pcmdevice_add_config(
	void *pContext, const unsigned char *config_data,
	unsigned int config_size)
{
	struct pcmdevice_priv *pcm_dev =
		(struct pcmdevice_priv *)pContext;
	struct pcmdevice_config_info *cfg_info = NULL;
	int config_offset = 0, i = 0;

	cfg_info = kzalloc(
			sizeof(struct pcmdevice_config_info), GFP_KERNEL);
	if (!cfg_info) {
		dev_err(pcm_dev->dev,
			"add config: cfg_info alloc failed!\n");
		goto out;
	}

	if (pcm_dev->mtRegbin.fw_hdr.binary_version_num >= 0x105) {
		if (config_offset + 64 > (int)config_size) {
			dev_err(pcm_dev->dev,
				"add config: Out of memory\n");
			goto out;
		}
		memcpy(cfg_info->mpName, &config_data[config_offset], 64);
		config_offset += 64;
	}

	if (config_offset + 4 > (int)config_size) {
		dev_err(pcm_dev->dev,
			"add config: Out of memory\n");
		goto out;
	}
	cfg_info->nblocks =
		get_unaligned_be32(&config_data[config_offset]);
	config_offset += 4;

	cfg_info->blk_data = kcalloc(
		cfg_info->nblocks, sizeof(struct pcmdevice_block_data *),
		GFP_KERNEL);
	if (!cfg_info->blk_data) {
		dev_err(pcm_dev->dev,
			"add config: blk_data alloc failed!\n");
		goto out;
	}
	cfg_info->real_nblocks = 0;
	for (i = 0; i < (int)cfg_info->nblocks; i++) {
		if (config_offset + 12 > config_size) {
			dev_err(pcm_dev->dev,
				"add config: Out of memory: i = %d "
				"nblocks = %u!\n", i,
				cfg_info->nblocks);
			break;
		}
		cfg_info->blk_data[i] = kzalloc(
			sizeof(struct pcmdevice_block_data), GFP_KERNEL);
		if (!cfg_info->blk_data[i]) {
			dev_err(pcm_dev->dev,
				"add config: blk_data[%d] alloc failed!\n", i);
			break;
		}
		cfg_info->blk_data[i]->dev_idx = config_data[config_offset];
		config_offset++;

		cfg_info->blk_data[i]->block_type = config_data[config_offset];
		config_offset++;

		if (cfg_info->blk_data[i]->block_type ==
			PCMDEVICE_BIN_BLK_PRE_POWER_UP) {
			if (cfg_info->blk_data[i]->dev_idx == 0) {
				cfg_info->active_dev = 1;
			} else {
				cfg_info->active_dev =
					(1 << (cfg_info->
					blk_data[i]->dev_idx - 1));
			}
		}
		cfg_info->blk_data[i]->yram_checksum =
			get_unaligned_be16(&config_data[config_offset]);
		config_offset += 2;
		cfg_info->blk_data[i]->block_size =
			get_unaligned_be32(&config_data[config_offset]);
		config_offset += 4;

		cfg_info->blk_data[i]->nSublocks =
			get_unaligned_be32(&config_data[config_offset]);

		config_offset += 4;
		cfg_info->blk_data[i]->regdata = kzalloc(
			cfg_info->blk_data[i]->block_size, GFP_KERNEL);
		if (!cfg_info->blk_data[i]->regdata) {
			dev_err(pcm_dev->dev,
				"add config: regdata alloc failed!\n");
			goto out;
		}
		if (config_offset + cfg_info->blk_data[i]->block_size >
			config_size) {
			dev_err(pcm_dev->dev,
				"add config: block_size Out of memory: "
				"i = %d nblocks = %u!\n",
				i, cfg_info->nblocks);
			break;
		}
		memcpy(cfg_info->blk_data[i]->regdata,
			&config_data[config_offset],
		cfg_info->blk_data[i]->block_size);
		config_offset += cfg_info->blk_data[i]->block_size;
		cfg_info->real_nblocks += 1;
	}
out:
	return cfg_info;
}

void pcmdevice_regbin_ready(const struct firmware *fmw,
	void *pContext)
{
	struct pcmdevice_priv *pcm_dev = (struct pcmdevice_priv *) pContext;
	struct pcmdevice_config_info **cfg_info = NULL;
	struct pcmdevice_regbin_hdr *fw_hdr = NULL;
	struct pcmdevice_regbin *regbin = NULL;
	unsigned int total_config_sz = 0;
	unsigned char *buf = NULL;
	int offset = 0, i = 0;

	if (pcm_dev == NULL) {
		dev_err(pcm_dev->dev,
			"pcmdev: regbin_ready: handle is NULL\n");
		return;
	}
	mutex_lock(&pcm_dev->codec_lock);
	regbin = &(pcm_dev->mtRegbin);
	fw_hdr = &(regbin->fw_hdr);
	if (!fmw || !fmw->data) {
		dev_err(pcm_dev->dev,
			"Failed to read %s, no side-effect on "
			"driver running\n", pcm_dev->regbinname);
		goto out;
	}
	buf = (unsigned char *)fmw->data;

	dev_info(pcm_dev->dev, "pcmdev: regbin_ready start\n");
	fw_hdr->img_sz = get_unaligned_be32(&buf[offset]);
	offset += 4;
	if (fw_hdr->img_sz != fmw->size) {
		dev_err(pcm_dev->dev,
			"File size not match, %d %u", (int)fmw->size,
			fw_hdr->img_sz);
		goto out;
	}

	fw_hdr->checksum = get_unaligned_be32(&buf[offset]);
	offset += 4;
	fw_hdr->binary_version_num = get_unaligned_be32(&buf[offset]);
	if (fw_hdr->binary_version_num < 0x103) {
		dev_err(pcm_dev->dev,
			"File version 0x%04x is too low",
			fw_hdr->binary_version_num);
		goto out;
	}
	offset += 4;
	fw_hdr->drv_fw_version = get_unaligned_be32(&buf[offset]);
	offset += 4;
	fw_hdr->timestamp = get_unaligned_be32(&buf[offset]);
	offset += 4;
	fw_hdr->plat_type = buf[offset];
	offset += 1;
	fw_hdr->dev_family = buf[offset];
	offset += 1;
	fw_hdr->reserve = buf[offset];
	offset += 1;
	fw_hdr->ndev = buf[offset];
	offset += 1;
	if (fw_hdr->ndev > MAX_DEV_NUM || fw_hdr->ndev == 0) {
		dev_err(pcm_dev->dev,
			"Invalid ndev(%u)\n", fw_hdr->ndev);
		goto out;
	}
	dev_info(pcm_dev->dev, "ndev = %u\n", (unsigned int)fw_hdr->ndev);
	if (offset + PCMDEVICE_DEVICE_SUM > fw_hdr->img_sz) {
		dev_err(pcm_dev->dev,
			"regbin_ready: Out of Memory!\n");
		goto out;
	}

	for (i = 0; i < PCMDEVICE_DEVICE_SUM; i++, offset++)
		fw_hdr->devs[i] = buf[offset];

	fw_hdr->nconfig = get_unaligned_be32(&buf[offset]);
	offset += 4;
	dev_info(pcm_dev->dev, "nconfig = %u\n", fw_hdr->nconfig);
	for (i = 0; i < PCMDEVICE_CONFIG_SUM; i++) {
		fw_hdr->config_size[i] = get_unaligned_be32(&buf[offset]);
		offset += 4;
		total_config_sz += fw_hdr->config_size[i];
	}
	dev_info(pcm_dev->dev,
		"img_sz = %u total_config_sz = %u offset = %d\n",
		fw_hdr->img_sz, total_config_sz, offset);
	if (fw_hdr->img_sz - total_config_sz != (unsigned int)offset) {
		dev_err(pcm_dev->dev, "Bin file error!\n");
		goto out;
	}
	cfg_info = kcalloc(fw_hdr->nconfig,
		sizeof(struct pcmdevice_config_info *),
		GFP_KERNEL);
	if (!cfg_info) {
		dev_err(pcm_dev->dev, "nconfig Memory alloc failed!\n");
		goto out;
	}
	regbin->cfg_info = cfg_info;
	regbin->ncfgs = 0;
	for (i = 0; i < (int)fw_hdr->nconfig; i++) {
		cfg_info[i] = pcmdevice_add_config(pContext, &buf[offset],
				fw_hdr->config_size[i]);
		if (!cfg_info[i]) {
			dev_err(pcm_dev->dev,
				"add_config Memory alloc failed!\n");
			break;
		}
		offset += (int)fw_hdr->config_size[i];
		regbin->ncfgs += 1;
	}

	pcmdevice_create_controls(pcm_dev);
out:
	mutex_unlock(&pcm_dev->codec_lock);
	if (fmw)
		release_firmware(fmw);
	dev_info(pcm_dev->dev, "Firmware init complete\n");
}

void pcmdevice_config_info_remove(void *ctxt)
{
	struct pcmdevice_priv *pcm_dev = (struct pcmdevice_priv *) ctxt;
	struct pcmdevice_regbin *regbin = &(pcm_dev->mtRegbin);
	struct pcmdevice_config_info **cfg_info = regbin->cfg_info;
	int i, j;

	if (!cfg_info)
		return;
	for (i = 0; i < regbin->ncfgs; i++) {
		if (!cfg_info[i])
			continue;
		if (cfg_info[i]->blk_data) {
			for (j = 0; j < (int)cfg_info[i]->real_nblocks; j++) {
				if (!cfg_info[i]->blk_data[j])
					continue;
				kfree(cfg_info[i]->blk_data[j]->regdata);
				kfree(cfg_info[i]->blk_data[j]);
			}
			kfree(cfg_info[i]->blk_data);
		}
		kfree(cfg_info[i]);
	}
	kfree(cfg_info);
}

MODULE_AUTHOR("Shenghao Ding <shenghao-ding@ti.com>");
MODULE_DESCRIPTION("ASoC PCMDEVICE Driver");
MODULE_LICENSE("GPL");
