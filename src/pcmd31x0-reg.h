/*
 * pcmdevice
 *
 * pcmdevice codec driver
 *
 * Copyright (C) 2020-2024 Texas Instruments Incorporated
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __PCMD31X0_REG_H__
#define __PCMD31X0_REG_H__

#define PCMD31X0_REG_INT_LTCH0	PCMDEVICE_REG(0X0, 0x36)

#endif
