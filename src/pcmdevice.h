/*
 * pcmdevice
 *
 * pcmdevice codec driver
 *
 * Copyright (C) 2020-2024 Texas Instruments Incorporated
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __PCMDEVICE_H__
#define __PCMDEVICE_H__
#include "pcmdevice-regbin.h"

#define MAX_DEV_NUM			2
#define PCMDEVICE_I2C_RETRY_COUNT	3
#define PCMDEVICE_ERROR_I2C_FAILED	-2
#define PCMDEVICE_REGBIN_FILENAME_LEN	32

#define PCMDEVICE_RATES	(SNDRV_PCM_RATE_44100 | \
	SNDRV_PCM_RATE_48000)
#define PCMDEVICE_MAX_CHANNELS (8)
#define PCMDEVICE_FORMATS	(SNDRV_PCM_FMTBIT_S16_LE | \
	SNDRV_PCM_FMTBIT_S20_3LE | \
	SNDRV_PCM_FMTBIT_S24_3LE | \
	SNDRV_PCM_FMTBIT_S24_LE | \
	SNDRV_PCM_FMTBIT_S32_LE)

/* PAGE Control Register (available in page0 of each book) */
#define PCMDEVICE_PAGE_SELECT			0x00
#define PCMDEVICE_PAGE_ID(reg)			(reg / 128)
#define PCMDEVICE_PAGE_REG(reg)			(reg % 128)
#define PCMDEVICE_REG(page, reg)		((page * 128) + reg)

/* Software Reset */
#define PCMDEVICE_REG_SWRESET			PCMDEVICE_REG(0X0, 0x01)
#define PCMDEVICE_REG_SWRESET_RESET		(0x1 << 0)
#define ADC5120_REG_CH1_ANALOG_GAIN		PCMDEVICE_REG(0X0, 0x3d)
#define ADC5120_REG_CH1_DIGITAL_GAIN	PCMDEVICE_REG(0X0, 0x3e)
#define ADC5120_REG_CH2_ANALOG_GAIN		PCMDEVICE_REG(0X0, 0x42)
#define ADC5120_REG_CH2_DIGITAL_GAIN	PCMDEVICE_REG(0X0, 0x43)
#define ADC5140_REG_CH3_ANALOG_GAIN		PCMDEVICE_REG(0X0, 0x47)
#define ADC5140_REG_CH3_DIGITAL_GAIN	PCMDEVICE_REG(0X0, 0x48)
#define ADC5140_REG_CH4_ANALOG_GAIN		PCMDEVICE_REG(0X0, 0x4C)
#define ADC5140_REG_CH4_DIGITAL_GAIN	PCMDEVICE_REG(0X0, 0x4C)

#define PCM1690_REG_CH1_DIGITAL_GAIN	PCMDEVICE_REG(0X0, 0x48)
#define PCM1690_REG_CH2_DIGITAL_GAIN	PCMDEVICE_REG(0X0, 0x49)
#define PCM1690_REG_CH3_DIGITAL_GAIN	PCMDEVICE_REG(0X0, 0x4a)
#define PCM1690_REG_CH4_DIGITAL_GAIN	PCMDEVICE_REG(0X0, 0x4b)
#define PCM1690_REG_CH5_DIGITAL_GAIN	PCMDEVICE_REG(0X0, 0x4c)
#define PCM1690_REG_CH6_DIGITAL_GAIN	PCMDEVICE_REG(0X0, 0x4d)
#define PCM1690_REG_CH7_DIGITAL_GAIN	PCMDEVICE_REG(0X0, 0x4e)
#define PCM1690_REG_CH8_DIGITAL_GAIN	PCMDEVICE_REG(0X0, 0x4f)

#define PCM186X_REG_CH1L_GAIN			PCMDEVICE_REG(0X0, 0x01)
#define PCM186X_REG_CH1R_GAIN			PCMDEVICE_REG(0X0, 0x02)
#define PCM186X_REG_CH2L_GAIN			PCMDEVICE_REG(0X0, 0x03)
#define PCM186X_REG_CH2R_GAIN			PCMDEVICE_REG(0X0, 0x04)

#define PCM6240_REG_CH1_ANALOG_GAIN		PCMDEVICE_REG(0X0, 0x3d)
#define PCM6240_REG_CH1_DIGITAL_GAIN	PCMDEVICE_REG(0X0, 0x3e)
#define PCM6240_REG_CH2_ANALOG_GAIN		PCMDEVICE_REG(0X0, 0x42)
#define PCM6240_REG_CH2_DIGITAL_GAIN	PCMDEVICE_REG(0X0, 0x43)
#define PCM6240_REG_CH3_ANALOG_GAIN		PCMDEVICE_REG(0X0, 0x47)
#define PCM6240_REG_CH3_DIGITAL_GAIN	PCMDEVICE_REG(0X0, 0x48)
#define PCM6240_REG_CH4_ANALOG_GAIN		PCMDEVICE_REG(0X0, 0x4c)
#define PCM6240_REG_CH4_DIGITAL_GAIN	PCMDEVICE_REG(0X0, 0x4d)

#define PCM6260_REG_CH1_ANALOG_GAIN		PCMDEVICE_REG(0X0, 0x3d)
#define PCM6260_REG_CH1_DIGITAL_GAIN	PCMDEVICE_REG(0X0, 0x3e)
#define PCM6260_REG_CH2_ANALOG_GAIN		PCMDEVICE_REG(0X0, 0x42)
#define PCM6260_REG_CH2_DIGITAL_GAIN	PCMDEVICE_REG(0X0, 0x43)
#define PCM6260_REG_CH3_ANALOG_GAIN		PCMDEVICE_REG(0X0, 0x47)
#define PCM6260_REG_CH3_DIGITAL_GAIN	PCMDEVICE_REG(0X0, 0x48)
#define PCM6260_REG_CH4_ANALOG_GAIN		PCMDEVICE_REG(0X0, 0x4c)
#define PCM6260_REG_CH4_DIGITAL_GAIN	PCMDEVICE_REG(0X0, 0x4d)
#define PCM6260_REG_CH5_ANALOG_GAIN		PCMDEVICE_REG(0X0, 0x51)
#define PCM6260_REG_CH5_DIGITAL_GAIN	PCMDEVICE_REG(0X0, 0x52)
#define PCM6260_REG_CH6_ANALOG_GAIN		PCMDEVICE_REG(0X0, 0x56)
#define PCM6260_REG_CH6_DIGITAL_GAIN	PCMDEVICE_REG(0X0, 0x57)

#define PCM9211_REG_CH1_DIGITAL_GAIN	PCMDEVICE_REG(0X0, 0x46)
#define PCM9211_REG_CH2_DIGITAL_GAIN	PCMDEVICE_REG(0X0, 0x47)

#define PCMD3140_REG_CH1_DIGITAL_GAIN	PCMDEVICE_REG(0X0, 0x3E)
#define PCMD3140_REG_CH2_DIGITAL_GAIN	PCMDEVICE_REG(0X0, 0x43)
#define PCMD3140_REG_CH3_DIGITAL_GAIN	PCMDEVICE_REG(0X0, 0x48)
#define PCMD3140_REG_CH4_DIGITAL_GAIN	PCMDEVICE_REG(0X0, 0x4D)

#define PCMD3180_REG_CH1_DIGITAL_GAIN	PCMDEVICE_REG(0X0, 0x3E)
#define PCMD3180_REG_CH2_DIGITAL_GAIN	PCMDEVICE_REG(0X0, 0x43)
#define PCMD3180_REG_CH3_DIGITAL_GAIN	PCMDEVICE_REG(0X0, 0x48)
#define PCMD3180_REG_CH4_DIGITAL_GAIN	PCMDEVICE_REG(0X0, 0x4D)
#define PCMD3180_REG_CH5_DIGITAL_GAIN	PCMDEVICE_REG(0X0, 0x52)
#define PCMD3180_REG_CH6_DIGITAL_GAIN	PCMDEVICE_REG(0X0, 0x57)
#define PCMD3180_REG_CH7_DIGITAL_GAIN	PCMDEVICE_REG(0X0, 0x5C)
#define PCMD3180_REG_CH8_DIGITAL_GAIN	PCMDEVICE_REG(0X0, 0x61)

#define PCM512X_REG_L_ANALOG_GAIN		PCMDEVICE_REG(0X1, 0x02)
#define PCM512X_REG_R_ANALOG_GAIN		PCMDEVICE_REG(0X1, 0x02)
#define PCM512X_REG_L_DIGITAL_GAIN		PCMDEVICE_REG(0X0, 0x3D)
#define PCM512X_REG_R_DIGITAL_GAIN		PCMDEVICE_REG(0X0, 0x3E)

#define TAA5421_REG_CH1_DIGITAL_VOLUME	PCMDEVICE_REG(0X0, 0x52)
#define TAA5421_REG_CH2_DIGITAL_VOLUME	PCMDEVICE_REG(0X0, 0x57)
#define TAA5421_REG_CH3_DIGITAL_VOLUME	PCMDEVICE_REG(0X0, 0x5B)
#define TAA5421_REG_CH4_DIGITAL_VOLUME	PCMDEVICE_REG(0X0, 0x5F)

#define TAA5421_REG_CH1_FINE_GAIN		PCMDEVICE_REG(0X0, 0x53)
#define TAA5421_REG_CH2_FINE_GAIN		PCMDEVICE_REG(0X0, 0x58)
#define TAA5421_REG_CH3_FINE_GAIN		PCMDEVICE_REG(0X0, 0x5C)
#define TAA5421_REG_CH4_FINE_GAIN		PCMDEVICE_REG(0X0, 0x60)

enum audio_device {
	ADC3120,
	ADC3140,
	ADC5120,
	ADC5140,
	ADC6120,
	ADC6140,
	DIX4192,
	PCM1690,
	PCM186X,
	PCM3120,
	PCM3140,
	PCM5120,
	PCM5140,
	PCM6120,
	PCM6140,
	PCM6240,
	PCM6260,
	PCM9211,
	PCMD3140,
	PCMD3180,
	PCM512X,
	TAA5412,
	TAD5212,
	MAX_DEVICE,
};

struct Trwinfo {
	int mnDBGCmd;
	int mnCurrentReg;
	int cur_chl;
	char mPage;
};

struct Tsyscmd {
	bool bCmdErr;
	unsigned char mdev;
	unsigned char mnPage;
	unsigned char mnReg;
	unsigned char mnValue;
	unsigned short bufLen;
	int cur_chl;
};

enum syscmds {
	RegSettingCmd = 0,
	RegDumpCmd    = 1,
	RegCfgListCmd = 2,
	MaxCmd
};

struct pcm_control {
	struct snd_kcontrol_new *pcmdevice_profile_controls;
	int nr_controls;
};

struct pcmdevice {
	unsigned int addr;
	bool is_loading;
};

struct pcmdevice_irqinfo {
	struct delayed_work irq_work;
	int irq_gpio;
	int irq;
	bool irq_enable;
};

struct pcmdevice_priv {
	struct snd_soc_component *component;
	struct i2c_client *client;
	struct device *dev;
	struct mutex dev_lock;
	struct mutex codec_lock;
	struct pcmdevice pcmdevice[MAX_DEV_NUM];
	struct regmap *regmap;
	struct Trwinfo rwinfo;
	struct Tsyscmd nSysCmd[MaxCmd];
	struct pcmdevice_regbin mtRegbin;
	struct pcm_control pcm_ctrl;
	struct pcmdevice_irqinfo irqinfo;
	int pstream;
	int cstream;
	unsigned int chip_id;
	struct gpio_desc *reset;
	int cur_conf;
	int ndev;
	unsigned char regbinname[PCMDEVICE_REGBIN_FILENAME_LEN];
	unsigned char dev_name[I2C_NAME_SIZE];
	bool runtime_suspend;
	void (*irq_work_func)(struct pcmdevice_priv *pcm_dev);
};

int pcmdevice_create_controls(
	struct pcmdevice_priv *pcm_dev);
void pcmdevice_codec_remove(
	struct snd_soc_component *codec);
void pcmdevice_enable_irq(struct pcmdevice_priv *pcm_dev,
	bool enable);

void adc5120_irq_work_func(struct pcmdevice_priv *pcm_dev);
void pcm186x_irq_work_func(struct pcmdevice_priv *pcm_dev);
void pcm6240_irq_work_func(struct pcmdevice_priv *pcm_dev);
void pcm6260_irq_work_func(struct pcmdevice_priv *pcm_dev);
void pcm9211_irq_work_func(struct pcmdevice_priv *pcm_dev);
void pcmd31x0_irq_work_func(struct pcmdevice_priv *pcm_dev);
void taa5412_irq_work_func(struct pcmdevice_priv *pcm_dev);
#endif /* __PCMDEVICE_H__ */
