/*
 * pcmdevice
 *
 * pcmdevice codec driver
 *
 * Copyright (C) 2020-2024 Texas Instruments Incorporated
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/firmware.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <sound/soc.h>

#include "pcmdevice.h"
#include "taa5412-reg.h"
#include "pcmdevice-rw.h"

void taa5412_irq_work_func(struct pcmdevice_priv *pcm_dev)
{
	unsigned int reg_val, array_size, i, index;
	unsigned int int_reg_array[] = {
		TAA5412_REG_INT_LTCH0,
		TAA5412_REG_CHX_LTCH,
		TAA5412_REG_IN_CH1_LTCH,
		TAA5412_REG_IN_CH2_LTCH,
		TAA5412_REG_INT_LTCH1,
		TAA5412_REG_INT_LTCH2
		};
	int rc;

	pcmdevice_enable_irq(pcm_dev, false);

	array_size = ARRAY_SIZE(int_reg_array);
	for (index = 0; index < pcm_dev->ndev; index++) {
		for (i = 0; i < array_size; i++) {
			rc = pcmdevice_dev_read(pcm_dev, index,
				int_reg_array[i], &reg_val);
			if (!rc)
				dev_info(pcm_dev->dev,
					"%s DEV_NO%d INT STATUS REG 0x%04x=0x%02x\n",
					pcm_dev->dev_name, index,
					int_reg_array[i], reg_val);
			else
				dev_err(pcm_dev->dev,
					"%s DEV_NO%d Read Reg 0x%04x error(rc=%d)\n",
					pcm_dev->dev_name, index,
					int_reg_array[i], rc);
		}
	}
}

MODULE_AUTHOR("Shenghao Ding <shenghao-ding@ti.com>");
MODULE_DESCRIPTION("ASoC PCMDEVICE Driver");
MODULE_LICENSE("GPL");
