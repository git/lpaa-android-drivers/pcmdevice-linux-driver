/*
 * pcmdevice
 *
 * pcmdevice codec driver
 *
 * Copyright (C) 2020-2024 Texas Instruments Incorporated
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __PCMDEVICE_RW_H__
#define __PCMDEVICE_RW_H__
int pcmdevice_dev_read(struct pcmdevice_priv *pPcmdev,
	unsigned int dev_no, unsigned int reg, unsigned int *pValue);

int pcmdevice_dev_write(struct pcmdevice_priv *pPcmdev,
	unsigned int dev_no, unsigned int reg, unsigned int value);

int pcmdevice_dev_bulk_write(
	struct pcmdevice_priv *pPcmdev, unsigned int dev_no, unsigned int reg,
	unsigned char *p_data, unsigned int n_length);

int pcmdevice_dev_bulk_read(struct pcmdevice_priv *pPcmdev,
	unsigned int dev_no, unsigned int reg, unsigned char *p_data,
	unsigned int n_length);

int pcmdevice_dev_update_bits(
	struct pcmdevice_priv *pPcmdev, unsigned int dev_no, unsigned int reg,
	unsigned int mask, unsigned int value);
#endif
