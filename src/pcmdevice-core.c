/*
 * pcmdevice
 *
 * pcmdevice codec driver
 *
 * Copyright (C) 2020-2024 Texas Instruments Incorporated
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/acpi.h>
#include <linux/firmware.h>
#include <linux/gpio/consumer.h>
#include <linux/i2c.h>
#include <linux/init.h>
#include <linux/moduleparam.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/of_gpio.h>
#include <linux/of_irq.h>
#include <linux/pm.h>
#include <linux/slab.h>
#include <linux/version.h>
#include <sound/core.h>
#include <sound/pcm.h>
#include <sound/pcm_params.h>
#include <sound/soc.h>
#include <sound/tlv.h>

#include "pcm9211-reg.h"
#include "pcmdevice.h"
#include "pcmdevice-node.h"
#include "pcmdevice-ops.h"
#include "pcmdevice-regbin.h"
#include "pcmdevice-rw.h"

/* max. length of a alsa mixer control name */
#define PCMDEVICE_CLK_DIR_IN		(0)
#define PCMDEVICE_CLK_DIR_OUT		(1)

static const struct i2c_device_id pcmdevice_i2c_id[] = {
	{ "adc3120", ADC3120	},
	{ "adc3140", ADC3140	},
	{ "adc5120", ADC5120	},
	{ "adc5140", ADC5140	},
	{ "adc6120", ADC6120	},
	{ "adc6140", ADC6140	},
	{ "dix4192", DIX4192	},
	{ "pcm1690", PCM1690	},
	{ "pcm186x", PCM186X	},
	{ "pcm3120", PCM3120	},
	{ "pcm3140", PCM3140	},
	{ "pcm5120", PCM5120	},
	{ "pcm5140", PCM5140	},
	{ "pcm6140", PCM6120	},
	{ "pcm6140", PCM6140	},
	{ "pcm6240", PCM6240	},
	{ "pcm6260", PCM6260	},
	{ "pcm9211", PCM9211	},
	{ "pcmd3140", PCMD3140	},
	{ "pcmd3180", PCMD3180	},
	{ "pcm512x", PCM512X	},
	{ "taa5412", TAA5412	},
	{ "tad5212", TAD5212	},
	{},
};

static const char *dts_tag[] = {
	"primary-device",
	"secondary-device",
};

static struct pcmdevice_mixer_control pcm186x_analog_gain_ctl[] = {
	{
		.shift = 0,
		.reg = PCM186X_REG_CH1L_GAIN,
		.max = 0x50,
		.invert = 0,
	},
	{
		.shift = 0,
		.reg = PCM186X_REG_CH1R_GAIN,
		.max = 0x50,
		.invert = 0,
	},
	{
		.shift = 0,
		.reg = PCM186X_REG_CH2L_GAIN,
		.max = 0x50,
		.invert = 0,
	},
	{
		.shift = 0,
		.reg = PCM186X_REG_CH2R_GAIN,
		.max = 0x50,
		.invert = 0,
	}
};

static struct pcmdevice_mixer_control pcm186x_digital_gain_ctl[] = {
	{
		.shift = 0,
		.reg = PCM186X_REG_CH1L_GAIN,
		.max = 0xff,
		.invert = 0,
	},
	{
		.shift = 0,
		.reg = PCM186X_REG_CH1R_GAIN,
		.max = 0xff,
		.invert = 0,
	},
	{
		.shift = 0,
		.reg = PCM186X_REG_CH2L_GAIN,
		.max = 0xff,
		.invert = 0,
	},
	{
		.shift = 0,
		.reg = PCM186X_REG_CH2R_GAIN,
		.max = 0xff,
		.invert = 0,
	}
};

static struct pcmdevice_mixer_control pcm512x_analog_gain_ctl[] = {
	{
		.shift = 1,
		.reg = PCM512X_REG_L_ANALOG_GAIN,
		.max = 0x1,
		.invert = 1,
	},
	{
		.shift = 4,
		.reg = PCM512X_REG_R_ANALOG_GAIN,
		.max = 0x1,
		.invert = 1,
	}
};

static struct pcmdevice_mixer_control pcm512x_digital_gain_ctl[] = {
	{
		.shift = 0,
		.reg = PCM512X_REG_L_DIGITAL_GAIN,
		.max = 0xff,
		.invert = 1,
	},
	{
		.shift = 0,
		.reg = PCM512X_REG_R_DIGITAL_GAIN,
		.max = 0xff,
		.invert = 1,
	}
};

static struct pcmdevice_mixer_control adc5120_analog_gain_ctl[] = {
	{
		.shift = 1,
		.reg = ADC5120_REG_CH1_ANALOG_GAIN,
		.max = 0x54,
		.invert = 0,
	},
	{
		.shift = 1,
		.reg = ADC5120_REG_CH2_ANALOG_GAIN,
		.max = 0x54,
		.invert = 0,
	}
};

static struct pcmdevice_mixer_control adc5120_digital_gain_ctl[] = {
	{
		.shift = 0,
		.reg = ADC5120_REG_CH1_DIGITAL_GAIN,
		.max = 0xff,
		.invert = 0,
	},
	{
		.shift = 0,
		.reg = ADC5120_REG_CH2_DIGITAL_GAIN,
		.max = 0xff,
		.invert = 0,
	}
};

static struct pcmdevice_mixer_control adc5140_analog_gain_ctl[] = {
	{
		.shift = 1,
		.reg = ADC5120_REG_CH1_ANALOG_GAIN,
		.max = 0x2a,
		.invert = 0,
	},
	{
		.shift = 1,
		.reg = ADC5120_REG_CH2_ANALOG_GAIN,
		.max = 0x2a,
		.invert = 0,
	},
	{
		.shift = 1,
		.reg = ADC5140_REG_CH3_ANALOG_GAIN,
		.max = 0x2a,
		.invert = 0,
	},
	{
		.shift = 1,
		.reg = ADC5140_REG_CH4_ANALOG_GAIN,
		.max = 0x2a,
		.invert = 0,
	}
};

static struct pcmdevice_mixer_control adc5140_digital_gain_ctl[] = {
	{
		.shift = 0,
		.reg = ADC5120_REG_CH1_DIGITAL_GAIN,
		.max = 0xff,
		.invert = 0,
	},
	{
		.shift = 0,
		.reg = ADC5120_REG_CH2_DIGITAL_GAIN,
		.max = 0xff,
		.invert = 0,
	},
	{
		.shift = 0,
		.reg = ADC5140_REG_CH3_DIGITAL_GAIN,
		.max = 0xff,
		.invert = 0,
	},
	{
		.shift = 0,
		.reg = ADC5140_REG_CH4_DIGITAL_GAIN,
		.max = 0xff,
		.invert = 0,
	}
};

static struct pcmdevice_mixer_control pcm1690_digital_gain_ctl[] = {
	{
		.shift = 0,
		.reg = PCM1690_REG_CH1_DIGITAL_GAIN,
		.max = 0xff,
		.invert = 0,
	},
	{
		.shift = 0,
		.reg = PCM1690_REG_CH2_DIGITAL_GAIN,
		.max = 0xff,
		.invert = 0,
	},
	{
		.shift = 0,
		.reg = PCM1690_REG_CH3_DIGITAL_GAIN,
		.max = 0xff,
		.invert = 0,
	},
	{
		.shift = 0,
		.reg = PCM1690_REG_CH4_DIGITAL_GAIN,
		.max = 0xff,
		.invert = 0,
	},
	{
		.shift = 0,
		.reg = PCM1690_REG_CH5_DIGITAL_GAIN,
		.max = 0xff,
		.invert = 0,
	},
	{
		.shift = 0,
		.reg = PCM1690_REG_CH6_DIGITAL_GAIN,
		.max = 0xff,
		.invert = 0,
	},
	{
		.shift = 0,
		.reg = PCM1690_REG_CH7_DIGITAL_GAIN,
		.max = 0xff,
		.invert = 0,
	},
	{
		.shift = 0,
		.reg = PCM1690_REG_CH8_DIGITAL_GAIN,
		.max = 0xff,
		.invert = 0,
	}
};

static struct pcmdevice_mixer_control pcm6240_analog_gain_ctl[] = {
	{
		.shift = 2,
		.reg = PCM6240_REG_CH1_ANALOG_GAIN,
		.max = 0x42,
		.invert = 0,
	},
	{
		.shift = 2,
		.reg = PCM6240_REG_CH2_ANALOG_GAIN,
		.max = 0x42,
		.invert = 0,
	},
	{
		.shift = 2,
		.reg = PCM6240_REG_CH3_ANALOG_GAIN,
		.max = 0x42,
		.invert = 0,
	},
	{
		.shift = 2,
		.reg = PCM6240_REG_CH4_ANALOG_GAIN,
		.max = 0x42,
		.invert = 0,
	}
};

static struct pcmdevice_mixer_control pcm6240_digital_gain_ctl[] = {
	{
		.shift = 0,
		.reg = PCM6240_REG_CH1_DIGITAL_GAIN,
		.max = 0xff,
		.invert = 0,
	},
	{
		.shift = 0,
		.reg = PCM6240_REG_CH2_DIGITAL_GAIN,
		.max = 0xff,
		.invert = 0,
	},
	{
		.shift = 0,
		.reg = PCM6240_REG_CH3_DIGITAL_GAIN,
		.max = 0xff,
		.invert = 0,
	},
	{
		.shift = 0,
		.reg = PCM6240_REG_CH4_DIGITAL_GAIN,
		.max = 0xff,
		.invert = 0,
	}
};

static struct pcmdevice_mixer_control pcm6260_analog_gain_ctl[] = {
	{
		.shift = 2,
		.reg = PCM6260_REG_CH1_ANALOG_GAIN,
		.max = 0x42,
		.invert = 0,
	},
	{
		.shift = 2,
		.reg = PCM6260_REG_CH2_ANALOG_GAIN,
		.max = 0x42,
		.invert = 0,
	},
	{
		.shift = 2,
		.reg = PCM6260_REG_CH3_ANALOG_GAIN,
		.max = 0x42,
		.invert = 0,
	},
	{
		.shift = 2,
		.reg = PCM6260_REG_CH4_ANALOG_GAIN,
		.max = 0x42,
		.invert = 0,
	},
	{
		.shift = 2,
		.reg = PCM6260_REG_CH5_ANALOG_GAIN,
		.max = 0x42,
		.invert = 0,
	},
	{
		.shift = 2,
		.reg = PCM6260_REG_CH6_ANALOG_GAIN,
		.max = 0x42,
		.invert = 0,
	}
};

static struct pcmdevice_mixer_control pcm6260_digital_gain_ctl[] = {
	{
		.shift = 0,
		.reg = PCM6260_REG_CH1_DIGITAL_GAIN,
		.max = 0xff,
		.invert = 0,
	},
	{
		.shift = 0,
		.reg = PCM6260_REG_CH2_DIGITAL_GAIN,
		.max = 0xff,
		.invert = 0,
	},
	{
		.shift = 0,
		.reg = PCM6260_REG_CH3_DIGITAL_GAIN,
		.max = 0xff,
		.invert = 0,
	},
	{
		.shift = 0,
		.reg = PCM6260_REG_CH4_DIGITAL_GAIN,
		.max = 0xff,
		.invert = 0,
	},
	{
		.shift = 0,
		.reg = PCM6260_REG_CH5_DIGITAL_GAIN,
		.max = 0xff,
		.invert = 0,
	},
	{
		.shift = 0,
		.reg = PCM6260_REG_CH6_DIGITAL_GAIN,
		.max = 0xff,
		.invert = 0,
	}
};

static struct pcmdevice_mixer_control pcm9211_digital_gain_ctl[] = {
	{
		.shift = 0,
		.reg = PCM9211_REG_CH1_DIGITAL_GAIN,
		.max = 0xff,
		.invert = 0,
	},
	{
		.shift = 0,
		.reg = PCM9211_REG_CH2_DIGITAL_GAIN,
		.max = 0xff,
		.invert = 0,
	}
};

static struct pcmdevice_mixer_control pcmd3140_digital_gain_ctl[] = {
	{
		.shift = 0,
		.reg = PCMD3140_REG_CH1_DIGITAL_GAIN,
		.max = 0xff,
		.invert = 0,
	},
	{
		.shift = 0,
		.reg = PCMD3140_REG_CH2_DIGITAL_GAIN,
		.max = 0xff,
		.invert = 0,
	},
	{
		.shift = 0,
		.reg = PCMD3140_REG_CH3_DIGITAL_GAIN,
		.max = 0xff,
		.invert = 0,
	},
	{
		.shift = 0,
		.reg = PCMD3140_REG_CH4_DIGITAL_GAIN,
		.max = 0xff,
		.invert = 0,
	}
};

static struct pcmdevice_mixer_control pcmd3180_digital_gain_ctl[] = {
	{
		.shift = 0,
		.reg = PCMD3180_REG_CH1_DIGITAL_GAIN,
		.max = 0xff,
		.invert = 0,
	},
	{
		.shift = 0,
		.reg = PCMD3180_REG_CH2_DIGITAL_GAIN,
		.max = 0xff,
		.invert = 0,
	},
	{
		.shift = 0,
		.reg = PCMD3180_REG_CH3_DIGITAL_GAIN,
		.max = 0xff,
		.invert = 0,
	},
	{
		.shift = 0,
		.reg = PCMD3180_REG_CH4_DIGITAL_GAIN,
		.max = 0xff,
		.invert = 0,
	},
	{
		.shift = 0,
		.reg = PCMD3180_REG_CH5_DIGITAL_GAIN,
		.max = 0xff,
		.invert = 0,
	},
	{
		.shift = 0,
		.reg = PCMD3180_REG_CH6_DIGITAL_GAIN,
		.max = 0xff,
		.invert = 0,
	},
	{
		.shift = 0,
		.reg = PCMD3180_REG_CH7_DIGITAL_GAIN,
		.max = 0xff,
		.invert = 0,
	},
	{
		.shift = 0,
		.reg = PCMD3180_REG_CH8_DIGITAL_GAIN,
		.max = 0xff,
		.invert = 0,
	}
};

static struct pcmdevice_mixer_control taa5412_digital_volume_ctl[] = {
	{
		.shift = 0,
		.reg = TAA5421_REG_CH1_DIGITAL_VOLUME,
		.max = 0xff,
		.invert = 0,
	},
	{
		.shift = 0,
		.reg = TAA5421_REG_CH2_DIGITAL_VOLUME,
		.max = 0xff,
		.invert = 0,
	},
	{
		.shift = 0,
		.reg = TAA5421_REG_CH3_DIGITAL_VOLUME,
		.max = 0xff,
		.invert = 0,
	},
	{
		.shift = 0,
		.reg = TAA5421_REG_CH4_DIGITAL_VOLUME,
		.max = 0xff,
		.invert = 0,
	},
};

static struct pcmdevice_mixer_control taa5412_fine_gain_ctl[] = {
	{
		.shift = 4,
		.reg = TAA5421_REG_CH1_FINE_GAIN,
		.max = 0xf,
		.invert = 0,
	},
	{
		.shift = 4,
		.reg = TAA5421_REG_CH2_FINE_GAIN,
		.max = 0xf,
		.invert = 0,
	},
	{
		.shift = 4,
		.reg = TAA5421_REG_CH3_FINE_GAIN,
		.max = 0xf,
		.invert = 4,
	},
	{
		.shift = 0,
		.reg = TAA5421_REG_CH4_FINE_GAIN,
		.max = 0xf,
		.invert = 4,
	},
};

static const DECLARE_TLV_DB_LINEAR(pcm186x_dig_gain_tlv, -1200, -50);
static const DECLARE_TLV_DB_LINEAR(pcm186x_chgain_tlv, 0, 4000);
static const DECLARE_TLV_DB_LINEAR(pcm512x_chgain_tlv, -600, 0);
static const DECLARE_TLV_DB_MINMAX_MUTE(pcm512x_dig_gain_tlv,
	-10300, 2400);
static const DECLARE_TLV_DB_MINMAX_MUTE(pcmd3180_dig_gain_tlv,
	-10000, 2700);
static const DECLARE_TLV_DB_MINMAX_MUTE(pcmd3140_dig_gain_tlv,
	-10000, 2700);
static const DECLARE_TLV_DB_MINMAX_MUTE(pcm1690_fine_dig_gain_tlv,
	-12750, 0);
static const DECLARE_TLV_DB_MINMAX_MUTE(pcm1690_dig_gain_tlv,
	-25500, 0);
static const DECLARE_TLV_DB_MINMAX_MUTE(pcm9211_dig_gain_tlv,
	-11450, 2000);
static const DECLARE_TLV_DB_MINMAX_MUTE(adc5120_fgain_tlv,
	-10050, 2700);
static const DECLARE_TLV_DB_LINEAR(adc5120_chgain_tlv, 0, 4200);
static const DECLARE_TLV_DB_MINMAX_MUTE(adc5140_fgain_tlv,
	-10050, 2700);
static const DECLARE_TLV_DB_LINEAR(adc5140_chgain_tlv, 0, 4200);
static const DECLARE_TLV_DB_MINMAX_MUTE(pcm6260_fgain_tlv,
	-10000, 2700);
static const DECLARE_TLV_DB_LINEAR(pcm6260_chgain_tlv, 0, 4200);
static const DECLARE_TLV_DB_MINMAX_MUTE(taa5412_dig_vol_tlv,
	-8050, 4700);
static const DECLARE_TLV_DB_LINEAR(taa5412_fine_gain_tlv,
	-80, 70);

static DEVICE_ATTR(reg, 0664, reg_show, reg_store);
static DEVICE_ATTR(regdump, 0664, regdump_show, regdump_store);
static DEVICE_ATTR(i2caddr, 0664, i2c_address_show, NULL);
static DEVICE_ATTR(regbininfo_list, 0664, regbininfo_list_show,
	NULL);
static DEVICE_ATTR(regcfg_list, 0664, regcfg_list_show,
	regcfg_list_store);
static DEVICE_ATTR(fwload, 0664, NULL, fwload_store);
static DEVICE_ATTR(device_list, 0664, device_list_show, NULL);

static struct attribute *sysfs_attrs[] = {
	&dev_attr_reg.attr,
	&dev_attr_regdump.attr,
	&dev_attr_i2caddr.attr,
	&dev_attr_regbininfo_list.attr,
	&dev_attr_regcfg_list.attr,
	&dev_attr_fwload.attr,
	&dev_attr_device_list.attr,
	NULL
};

//nodes are in /sys/devices/platform/XXXXXXXX.i2cX/i2c-X/
// Or /sys/bus/i2c/devices/7-004c/
const struct attribute_group pcmdevice_attribute_group = {
	.attrs = sysfs_attrs
};

static const struct regmap_range_cfg pcmdevice_ranges[] = {
	{
		.range_min = 0,
		.range_max = 256 * 128,
		.selector_reg = PCMDEVICE_PAGE_SELECT,
		.selector_mask = 0xff,
		.selector_shift = 0,
		.window_start = 0,
		.window_len = 128,
	},
};

static const struct regmap_config pcmdevice_i2c_regmap = {
	.reg_bits = 8,
	.val_bits = 8,
	.cache_type = REGCACHE_RBTREE,
	.ranges = pcmdevice_ranges,
	.num_ranges = ARRAY_SIZE(pcmdevice_ranges),
	.max_register = 256 * 128,
};

static const struct snd_soc_dapm_widget pcmdevice_dapm_widgets[] = {
	SND_SOC_DAPM_AIF_IN("ASI", "ASI Playback", 0, SND_SOC_NOPM, 0, 0),
	SND_SOC_DAPM_AIF_OUT("ASI1 OUT", "ASI1 Capture",
		0, SND_SOC_NOPM, 0, 0),
	SND_SOC_DAPM_OUTPUT("OUT"),
	SND_SOC_DAPM_INPUT("AMIC"),
};

static const struct snd_soc_dapm_route pcmdevice_audio_map[] = {
	{"OUT", NULL, "ASI"},
	{"ASI1 OUT", NULL, "AMIC"},
};

static int pcmdevice_info_profile(
	struct snd_kcontrol *kcontrol,
	struct snd_ctl_elem_info *uinfo)
{
	struct snd_soc_component *codec
		= snd_soc_kcontrol_component(kcontrol);
	struct pcmdevice_priv *pcm_dev =
		snd_soc_component_get_drvdata(codec);

	uinfo->type = SNDRV_CTL_ELEM_TYPE_INTEGER;
	uinfo->count = 1;
	uinfo->value.integer.min = 0;
	uinfo->value.integer.max = max(0, pcm_dev->mtRegbin.ncfgs - 1);

	return 0;
}

static int pcmdevice_get_profile_id(
	struct snd_kcontrol *kcontrol,
	struct snd_ctl_elem_value *ucontrol)
{
	struct snd_soc_component *codec
		= snd_soc_kcontrol_component(kcontrol);
	struct pcmdevice_priv *pcm_dev =
		snd_soc_component_get_drvdata(codec);

	ucontrol->value.integer.value[0] = pcm_dev->cur_conf;

	return 0;
}

static int pcmdevice_set_profile_id(
	struct snd_kcontrol *kcontrol,
	struct snd_ctl_elem_value *ucontrol)
{
	struct snd_soc_component *codec
		= snd_soc_kcontrol_component(kcontrol);
	struct pcmdevice_priv *pcm_dev =
		snd_soc_component_get_drvdata(codec);
	int ret = 0;

	if (pcm_dev->cur_conf != ucontrol->value.integer.value[0]) {
		pcm_dev->cur_conf = ucontrol->value.integer.value[0];
		ret = 1;
	}

	return ret;
}

static int pcm1690_ctrl_add(struct pcmdevice_priv *pcm_dev)
{
	struct snd_kcontrol_new *pcmdevice_profile_controls = NULL;
	struct pcmdev_ctrl_info fine_dig_ctl_info = {0};
	int nr_controls = 1, ret = 0, mix_index = 0;
	struct pcmdev_ctrl_info dig_ctl_info = {0};
	char *name = NULL;
	int i, chn;

	dig_ctl_info.gain = pcm1690_dig_gain_tlv;
	dig_ctl_info.pcmdev_ctrl = pcm1690_digital_gain_ctl;
	dig_ctl_info.ctrl_array_size =
		ARRAY_SIZE(pcm1690_digital_gain_ctl);

	fine_dig_ctl_info.gain = pcm1690_fine_dig_gain_tlv;
	fine_dig_ctl_info.pcmdev_ctrl = pcm1690_digital_gain_ctl;
	fine_dig_ctl_info.ctrl_array_size =
		ARRAY_SIZE(pcm1690_digital_gain_ctl);
	nr_controls += pcm_dev->ndev * (dig_ctl_info.ctrl_array_size +
		fine_dig_ctl_info.ctrl_array_size);

	pcmdevice_profile_controls = devm_kzalloc(pcm_dev->dev,
		nr_controls * sizeof(*pcmdevice_profile_controls),
		GFP_KERNEL);
	if (pcmdevice_profile_controls == NULL) {
		dev_err(pcm_dev->dev,
			"%s: create kcontrol err\n", __func__);
		ret = -ENOMEM;
		pcm_dev->pcm_ctrl.pcmdevice_profile_controls = NULL;
		goto out;
	}
	pcm_dev->pcm_ctrl.pcmdevice_profile_controls =
		pcmdevice_profile_controls;
	/* Create a mixer item for selecting the active profile */
	name = devm_kzalloc(pcm_dev->dev,
		SNDRV_CTL_ELEM_ID_NAME_MAXLEN, GFP_KERNEL);
	if (!name) {
		dev_err(pcm_dev->dev,
			"%s: fail to name the kcontrol\n", __func__);
		ret = -ENOMEM;
		goto out;
	}
	scnprintf(name, SNDRV_CTL_ELEM_ID_NAME_MAXLEN,
		"PCMDEVICE Profile id");
	pcmdevice_profile_controls[mix_index].name = name;
	pcmdevice_profile_controls[mix_index].iface =
		SNDRV_CTL_ELEM_IFACE_MIXER;
	pcmdevice_profile_controls[mix_index].info =
		pcmdevice_info_profile;
	pcmdevice_profile_controls[mix_index].get =
		pcmdevice_get_profile_id;
	pcmdevice_profile_controls[mix_index].put =
		pcmdevice_set_profile_id;
	mix_index++;

	if (dig_ctl_info.ctrl_array_size != 0) {
		for(i = 0; i < pcm_dev->ndev; i++) {
			if(mix_index >= nr_controls) {
				dev_info(pcm_dev->dev,
					"%s: mix_index = %d nr_controls = %d\n",
					__func__, mix_index, nr_controls);
				break;
			}
			for (chn = 1; chn <= dig_ctl_info.ctrl_array_size;
				chn++) {
				if(mix_index >= nr_controls) {
					dev_info(pcm_dev->dev,
						"%s: mix_index = %d nr_controls = %d\n",
						__func__, mix_index,
						nr_controls);
					break;
				}
				name = devm_kzalloc(pcm_dev->dev,
					SNDRV_CTL_ELEM_ID_NAME_MAXLEN,
					GFP_KERNEL);
				if (!name) {
					dev_err(pcm_dev->dev,
						"%s: fail to name the "
						"kcontrol\n", __func__);
					ret = -ENOMEM;
					goto out;
				}
				scnprintf(name, SNDRV_CTL_ELEM_ID_NAME_MAXLEN,
					"dev%d-ch%d-digital-gain", i, chn);
				pcmdevice_profile_controls[mix_index].tlv.p =
					dig_ctl_info.gain;
				dig_ctl_info.pcmdev_ctrl[chn - 1].dev_no = i;
				pcmdevice_profile_controls[mix_index].
					private_value =
						(unsigned long)&dig_ctl_info.pcmdev_ctrl
						[chn - 1];
				pcmdevice_profile_controls[mix_index].name =
					name;
				pcmdevice_profile_controls[mix_index].access =
					SNDRV_CTL_ELEM_ACCESS_TLV_READ |
			 		SNDRV_CTL_ELEM_ACCESS_READWRITE;
				pcmdevice_profile_controls[mix_index].iface =
					SNDRV_CTL_ELEM_IFACE_MIXER;
				pcmdevice_profile_controls[mix_index].info =
					pcmdevice_info_volsw;
				pcmdevice_profile_controls[mix_index].get =
					pcm1690_get_volsw;
				pcmdevice_profile_controls[mix_index].put =
					pcm1690_put_volsw;
				mix_index++;
			}
		}
	}

	if (fine_dig_ctl_info.ctrl_array_size != 0) {
		for(i = 0; i < pcm_dev->ndev; i++) {
			if(mix_index >= nr_controls) {
				dev_info(pcm_dev->dev,
					"%s: mix_index = %d nr_controls = %d\n",
					__func__, mix_index, nr_controls);
				break;
			}
			for (chn = 1; chn <= fine_dig_ctl_info.ctrl_array_size;
				chn++) {
				if(mix_index >= nr_controls) {
					dev_info(pcm_dev->dev,
						"%s: mix_index = %d nr_controls = %d\n",
						__func__, mix_index,
						nr_controls);
					break;
				}
				name = devm_kzalloc(pcm_dev->dev,
					SNDRV_CTL_ELEM_ID_NAME_MAXLEN,
					GFP_KERNEL);
				if (!name) {
					dev_err(pcm_dev->dev,
						"%s: fail to name the "
						"kcontrol\n", __func__);
					ret = -ENOMEM;
					goto out;
				}
				scnprintf(name, SNDRV_CTL_ELEM_ID_NAME_MAXLEN,
					"dev%d-ch%d-fine-digital-gain", i, chn);
				pcmdevice_profile_controls[mix_index].tlv.p =
					fine_dig_ctl_info.gain;
				fine_dig_ctl_info.pcmdev_ctrl[chn - 1].dev_no = i;
				pcmdevice_profile_controls[mix_index].
					private_value =
						(unsigned long)&fine_dig_ctl_info.pcmdev_ctrl
						[chn - 1];
				pcmdevice_profile_controls[mix_index].name =
					name;
				pcmdevice_profile_controls[mix_index].access =
					SNDRV_CTL_ELEM_ACCESS_TLV_READ |
			 		SNDRV_CTL_ELEM_ACCESS_READWRITE;
				pcmdevice_profile_controls[mix_index].iface =
					SNDRV_CTL_ELEM_IFACE_MIXER;
				pcmdevice_profile_controls[mix_index].info =
					pcmdevice_info_volsw;
				pcmdevice_profile_controls[mix_index].get =
					pcm1690_get_finevolsw;
				pcmdevice_profile_controls[mix_index].put =
					pcm1690_put_finevolsw;
				mix_index++;
			}
		}
	}

	ret = snd_soc_add_component_controls(pcm_dev->component,
		pcmdevice_profile_controls,
		nr_controls < mix_index ? nr_controls : mix_index);
	if(ret)
		dev_err(pcm_dev->dev,
			"%s: add_component_controls error = %d\n",
			__func__, ret);
	pcm_dev->pcm_ctrl.nr_controls =
		nr_controls < mix_index ? nr_controls : mix_index;
out:
	return ret;
}

int pcmdevice_create_controls(struct pcmdevice_priv *pcm_dev)
{
	int nr_controls = 1, ret = 0, mix_index = 0, dev, chn = 1;
	struct snd_kcontrol_new *pcmdevice_profile_controls = NULL;
	struct pcmdev_ctrl_info analog_ctl_info = {0};
	struct pcmdev_ctrl_info dig_ctl_info = {0};
	char *name = NULL;

	switch (pcm_dev->chip_id) {
	case ADC3120:
	case ADC5120:
	case ADC6120:
	case PCM3120:
	case PCM5120:
	case PCM6120:
		dig_ctl_info.gain = adc5120_fgain_tlv;
		analog_ctl_info.gain = adc5120_chgain_tlv;
		analog_ctl_info.pcmdev_ctrl = adc5120_analog_gain_ctl;
		analog_ctl_info.ctrl_array_size =
			ARRAY_SIZE(adc5120_analog_gain_ctl);
		dig_ctl_info.pcmdev_ctrl = adc5120_digital_gain_ctl;
		dig_ctl_info.ctrl_array_size =
			ARRAY_SIZE(adc5120_digital_gain_ctl);
		break;
	case ADC3140:
	case ADC5140:
	case ADC6140:
		dig_ctl_info.gain = adc5140_fgain_tlv;
		analog_ctl_info.gain = adc5140_chgain_tlv;
		analog_ctl_info.pcmdev_ctrl = adc5140_analog_gain_ctl;
		analog_ctl_info.ctrl_array_size =
			ARRAY_SIZE(adc5140_analog_gain_ctl);
		dig_ctl_info.pcmdev_ctrl = adc5140_digital_gain_ctl;
		dig_ctl_info.ctrl_array_size =
			ARRAY_SIZE(adc5140_digital_gain_ctl);
		break;
	case PCM1690:
		ret = pcm1690_ctrl_add(pcm_dev);
		goto out;
		break;
	case PCM186X:
		analog_ctl_info.gain = pcm186x_chgain_tlv;
		analog_ctl_info.pcmdev_ctrl = pcm186x_analog_gain_ctl;
		analog_ctl_info.ctrl_array_size =
			ARRAY_SIZE(pcm186x_analog_gain_ctl);
		dig_ctl_info.gain = pcm186x_dig_gain_tlv;
		dig_ctl_info.pcmdev_ctrl = pcm186x_digital_gain_ctl;
		dig_ctl_info.ctrl_array_size =
			ARRAY_SIZE(pcm186x_digital_gain_ctl);
		break;
	case PCM3140:
	case PCM5140:
	case PCM6140:
	case PCM6240:
		dig_ctl_info.gain = pcm6260_fgain_tlv;
		analog_ctl_info.gain = pcm6260_chgain_tlv;
		analog_ctl_info.pcmdev_ctrl = pcm6240_analog_gain_ctl;
		analog_ctl_info.ctrl_array_size =
			ARRAY_SIZE(pcm6240_analog_gain_ctl);
		dig_ctl_info.pcmdev_ctrl = pcm6240_digital_gain_ctl;
		dig_ctl_info.ctrl_array_size =
			ARRAY_SIZE(pcm6240_digital_gain_ctl);
		break;
	case PCM6260:
		dig_ctl_info.gain = pcm6260_fgain_tlv;
		analog_ctl_info.gain = pcm6260_chgain_tlv;
		analog_ctl_info.pcmdev_ctrl = pcm6260_analog_gain_ctl;
		analog_ctl_info.ctrl_array_size =
			ARRAY_SIZE(pcm6260_analog_gain_ctl);
		dig_ctl_info.pcmdev_ctrl = pcm6260_digital_gain_ctl;
		dig_ctl_info.ctrl_array_size =
			ARRAY_SIZE(pcm6260_digital_gain_ctl);
		break;
	case PCM9211:
		dig_ctl_info.gain = pcm9211_dig_gain_tlv;
		dig_ctl_info.pcmdev_ctrl = pcm9211_digital_gain_ctl;
		dig_ctl_info.ctrl_array_size =
			ARRAY_SIZE(pcm9211_digital_gain_ctl);
		break;
	case PCMD3140:
		dig_ctl_info.gain = pcmd3140_dig_gain_tlv;
		dig_ctl_info.pcmdev_ctrl = pcmd3140_digital_gain_ctl;
		dig_ctl_info.ctrl_array_size =
			ARRAY_SIZE(pcmd3140_digital_gain_ctl);
		break;
	case PCMD3180:
		dig_ctl_info.gain = pcmd3180_dig_gain_tlv;
		dig_ctl_info.pcmdev_ctrl = pcmd3180_digital_gain_ctl;
		dig_ctl_info.ctrl_array_size =
			ARRAY_SIZE(pcmd3180_digital_gain_ctl);
		break;
	case PCM512X:
		analog_ctl_info.gain = pcm512x_chgain_tlv;
		analog_ctl_info.pcmdev_ctrl = pcm512x_analog_gain_ctl;
		analog_ctl_info.ctrl_array_size =
			ARRAY_SIZE(pcm512x_analog_gain_ctl);
		dig_ctl_info.gain = pcm512x_dig_gain_tlv;
		dig_ctl_info.pcmdev_ctrl = pcm512x_digital_gain_ctl;
		dig_ctl_info.ctrl_array_size =
			ARRAY_SIZE(pcm512x_digital_gain_ctl);
		break;
	case TAA5412:
		analog_ctl_info.gain = taa5412_fine_gain_tlv;
		analog_ctl_info.pcmdev_ctrl = taa5412_fine_gain_ctl;
		analog_ctl_info.ctrl_array_size =
			ARRAY_SIZE(taa5412_fine_gain_ctl);
		dig_ctl_info.gain = taa5412_dig_vol_tlv;
		dig_ctl_info.pcmdev_ctrl = taa5412_digital_volume_ctl;
		dig_ctl_info.ctrl_array_size =
			ARRAY_SIZE(taa5412_digital_volume_ctl);
		break;
	}

	nr_controls += pcm_dev->ndev * (dig_ctl_info.ctrl_array_size +
		analog_ctl_info.ctrl_array_size);
	pcmdevice_profile_controls = devm_kzalloc(pcm_dev->dev,
		nr_controls * sizeof(struct snd_kcontrol_new),
		GFP_KERNEL);
	if (pcmdevice_profile_controls == NULL) {
		dev_err(pcm_dev->dev,
			"%s: create kcontrol err\n", __func__);
		ret = -ENOMEM;
		pcm_dev->pcm_ctrl.pcmdevice_profile_controls = NULL;
		goto out;
	}
	pcm_dev->pcm_ctrl.pcmdevice_profile_controls =
		pcmdevice_profile_controls;
	/* Create a mixer item for selecting the active profile */
	name = devm_kzalloc(pcm_dev->dev,
		SNDRV_CTL_ELEM_ID_NAME_MAXLEN, GFP_KERNEL);
	if (!name) {
		dev_err(pcm_dev->dev,
			"%s: fail to name the kcontrol\n", __func__);
		ret = -ENOMEM;
		goto out;
	}
	scnprintf(name, SNDRV_CTL_ELEM_ID_NAME_MAXLEN,
		"PCMDEVICE Profile id");
	pcmdevice_profile_controls[mix_index].name = name;
	pcmdevice_profile_controls[mix_index].iface =
		SNDRV_CTL_ELEM_IFACE_MIXER;
	pcmdevice_profile_controls[mix_index].info =
		pcmdevice_info_profile;
	pcmdevice_profile_controls[mix_index].get =
		pcmdevice_get_profile_id;
	pcmdevice_profile_controls[mix_index].put =
		pcmdevice_set_profile_id;
	mix_index++;

	if( analog_ctl_info.ctrl_array_size != 0 ) {
		for(dev = 0; dev < pcm_dev->ndev; dev++) {
			if(mix_index >= nr_controls) {
				dev_info(pcm_dev->dev,
					"%s: mix_index = %d nr_controls = %d\n",
					__func__, mix_index, nr_controls);
				break;
			}
			for(chn = 1; chn <= analog_ctl_info.ctrl_array_size;
				chn++) {
				if(mix_index >= nr_controls) {
					dev_info(pcm_dev->dev,
						"%s: mix_index = %d nr_controls = %d\n",
						__func__, mix_index,
						nr_controls);
					break;
				}
				name = devm_kzalloc(pcm_dev->dev,
					SNDRV_CTL_ELEM_ID_NAME_MAXLEN,
					GFP_KERNEL);
				if (!name) {
					dev_err(pcm_dev->dev,
						"%s: fail to name the "
						"kcontrol\n", __func__);
					ret = -ENOMEM;
					goto out;
				}
				scnprintf(name, SNDRV_CTL_ELEM_ID_NAME_MAXLEN,
					(pcm_dev->chip_id != TAA5412)? "dev%d-ch%d-analog-gain" :
					"dev%d-ch%d-fine-gain", dev, chn);
				pcmdevice_profile_controls[mix_index].tlv.p =
					analog_ctl_info.gain;
				analog_ctl_info.pcmdev_ctrl[chn - 1].dev_no = dev;
				pcmdevice_profile_controls[mix_index].private_value =
					(unsigned long)&analog_ctl_info.pcmdev_ctrl[chn - 1];
				pcmdevice_profile_controls[mix_index].name = name;
				pcmdevice_profile_controls[mix_index].access =
					SNDRV_CTL_ELEM_ACCESS_TLV_READ |
			 		SNDRV_CTL_ELEM_ACCESS_READWRITE;
				pcmdevice_profile_controls[mix_index].iface =
					SNDRV_CTL_ELEM_IFACE_MIXER;
				pcmdevice_profile_controls[mix_index].info =
					pcmdevice_info_volsw;
				pcmdevice_profile_controls[mix_index].get =
					pcmdevice_get_volsw;
				pcmdevice_profile_controls[mix_index].put =
					pcmdevice_put_volsw;
				mix_index++;
			}
		}
	}

	if (dig_ctl_info.ctrl_array_size != 0) {
		for(dev = 0; dev < pcm_dev->ndev; dev++) {
			if(mix_index >= nr_controls) {
				dev_info(pcm_dev->dev, "%s: mix_index = %d nr_controls = %d\n",
					__func__, mix_index, nr_controls);
				break;
			}
			for (chn = 1; chn <= dig_ctl_info.ctrl_array_size; chn++) {
				if(mix_index >= nr_controls) {
					dev_info(pcm_dev->dev,
						"%s: mix_index = %d nr_controls = %d\n", __func__,
						mix_index, nr_controls);
					break;
				}
				name = devm_kzalloc(pcm_dev->dev,
					SNDRV_CTL_ELEM_ID_NAME_MAXLEN,
					GFP_KERNEL);
				if (!name) {
					dev_err(pcm_dev->dev, "%s: fail to name the kcontrol\n",
						__func__);
					ret = -ENOMEM;
					goto out;
				}
				scnprintf(name, SNDRV_CTL_ELEM_ID_NAME_MAXLEN,
					"dev%d-ch%d-digital-gain", dev, chn);
				pcmdevice_profile_controls[mix_index].tlv.p =
					dig_ctl_info.gain;
				dig_ctl_info.pcmdev_ctrl[chn - 1].dev_no = dev;
				pcmdevice_profile_controls[mix_index].private_value =
						(unsigned long)&dig_ctl_info.pcmdev_ctrl[chn - 1];
				pcmdevice_profile_controls[mix_index].name =
					name;
				pcmdevice_profile_controls[mix_index].access =
					SNDRV_CTL_ELEM_ACCESS_TLV_READ |
			 		SNDRV_CTL_ELEM_ACCESS_READWRITE;
				pcmdevice_profile_controls[mix_index].iface =
					SNDRV_CTL_ELEM_IFACE_MIXER;
				pcmdevice_profile_controls[mix_index].info =
					pcmdevice_info_volsw;
				pcmdevice_profile_controls[mix_index].get =
					pcmdevice_get_volsw;
				pcmdevice_profile_controls[mix_index].put =
					pcmdevice_put_volsw;
				mix_index++;
			}
		}
	}

	ret = snd_soc_add_component_controls(pcm_dev->component,
		pcmdevice_profile_controls,
		nr_controls < mix_index ? nr_controls : mix_index);
	if(ret)
		dev_err(pcm_dev->dev, "%s: add_component_controls error = %d\n",
			__func__, ret);
	pcm_dev->pcm_ctrl.nr_controls =
		nr_controls < mix_index ? nr_controls : mix_index;
out:
	return ret;
}

void pcmdevice_enable_irq(struct pcmdevice_priv *pcm_dev,
	bool is_enable)
{
	if (is_enable == pcm_dev->irqinfo.irq_enable &&
		!gpio_is_valid(pcm_dev->irqinfo.irq_gpio))
		return;

	if (is_enable)
		enable_irq(pcm_dev->irqinfo.irq);
	else
		disable_irq_nosync(pcm_dev->irqinfo.irq);

	pcm_dev->irqinfo.irq_enable = is_enable;
}

static void irq_work_routine(struct work_struct *work)
{
	struct pcmdevice_priv *pcm_dev =
		container_of(work, struct pcmdevice_priv, irqinfo.irq_work.work);

	dev_info(pcm_dev->dev, "%s enter\n", __func__);

	mutex_lock(&pcm_dev->codec_lock);
	if (pcm_dev->runtime_suspend) {
		dev_info(pcm_dev->dev, "%s, Runtime Suspended\n", __func__);
		goto end;
	}

	/*Logical Layer IRQ function, return is ignored*/
	if (pcm_dev->irq_work_func)
		pcm_dev->irq_work_func(pcm_dev);
	else
		dev_info(pcm_dev->dev,
			"%s, irq_work_func is NULL\n", __func__);

end:
	mutex_unlock(&pcm_dev->codec_lock);
	dev_info(pcm_dev->dev, "%s leave\n", __func__);
}

static irqreturn_t pcmdevice_irq_handler(int irq,
	void *dev_id)
{
	struct pcmdevice_priv *pcm_dev = (struct pcmdevice_priv *)dev_id;
;
	/* get IRQ status after 100 ms */
	schedule_delayed_work(&pcm_dev->irqinfo.irq_work, msecs_to_jiffies(100));
	return IRQ_HANDLED;
}

static int pcmdevice_codec_probe(
	struct snd_soc_component *component)
{
	struct pcmdevice_priv *pcm_dev =
		snd_soc_component_get_drvdata(component);
	int ret, i;

	mutex_lock(&pcm_dev->codec_lock);
	pcm_dev->component = component;

	strcpy(pcm_dev->dev_name, pcmdevice_i2c_id[pcm_dev->chip_id].name);

	scnprintf(pcm_dev->regbinname, PCMDEVICE_REGBIN_FILENAME_LEN,
		"%s-%udev-reg.bin", pcm_dev->dev_name, pcm_dev->ndev);

	ret = request_firmware_nowait(THIS_MODULE,
#if KERNEL_VERSION(6, 2, 0) <= LINUX_VERSION_CODE
		FW_ACTION_UEVENT
#else
		FW_ACTION_HOTPLUG
#endif
		, pcm_dev->regbinname, pcm_dev->dev, GFP_KERNEL, pcm_dev,
		pcmdevice_regbin_ready);
	if (ret) {
		dev_err(pcm_dev->dev, "load %s error = %d\n",
			pcm_dev->regbinname, ret);
		goto out;
	}

	if (pcm_dev->reset) {
		gpiod_set_value_cansleep(pcm_dev->reset, 0);
		usleep_range(500, 1000);
		gpiod_set_value_cansleep(pcm_dev->reset, 1);
	} else {
		for (i = 0; i < pcm_dev->ndev; i++) {
			if (pcm_dev->chip_id == PCM9211 ||
				pcm_dev->chip_id == PCM1690)
				ret = pcmdevice_dev_update_bits(pcm_dev, i,
					PCM9211_REG_SW_CTRL,
					PCM9211_REG_SW_CTRL_MRST_MSK,
					PCM9211_REG_SW_CTRL_MRST);
			else
				ret = pcmdevice_dev_write(pcm_dev, i,
					PCMDEVICE_REG_SWRESET,
					PCMDEVICE_REG_SWRESET_RESET);
			if (ret < 0)
				dev_err(pcm_dev->dev,
					"dev %d swreset fail, %d\n",
					i, ret);
		}
	}

out:
	mutex_unlock(&pcm_dev->codec_lock);
	return ret;
}

static int pcmdevice_mute(struct snd_soc_dai *dai, int mute,
	int stream)
{
	struct snd_soc_component *codec = dai->component;
	struct pcmdevice_priv *pcm_dev =
		snd_soc_component_get_drvdata(codec);
	mutex_lock(&pcm_dev->codec_lock);

	if (mute) {
		if (stream == SNDRV_PCM_STREAM_PLAYBACK)
			pcm_dev->pstream = 0;
		else
			pcm_dev->cstream = 0;
		if(!(pcm_dev->pstream || pcm_dev->cstream)) {
			if (pcm_dev->irq_work_func != NULL)
				pcmdevice_enable_irq(pcm_dev, false);
			pcmdevice_select_cfg_blk(pcm_dev, pcm_dev->cur_conf,
				PCMDEVICE_BIN_BLK_PRE_SHUTDOWN);
		}
	} else {
		if (stream == SNDRV_PCM_STREAM_PLAYBACK)
			pcm_dev->pstream = 1;
		else
			pcm_dev->cstream = 1;
		pcmdevice_select_cfg_blk(pcm_dev, pcm_dev->cur_conf,
			PCMDEVICE_BIN_BLK_PRE_POWER_UP);
		if (pcm_dev->irq_work_func != NULL)
			pcmdevice_enable_irq(pcm_dev, true);
	}

	mutex_unlock(&pcm_dev->codec_lock);
	return 0;
}

void pcmdevice_codec_remove(struct snd_soc_component *codec)
{
	struct pcmdevice_priv *pcm_dev =
		snd_soc_component_get_drvdata(codec);
	int mix_index = 0;

	if (pcm_dev) {
		mutex_lock(&pcm_dev->codec_lock);
		if (pcm_dev->irq_work_func != NULL)
			if (gpio_is_valid(pcm_dev->irqinfo.irq_gpio)) {
				if (delayed_work_pending(&pcm_dev->irqinfo.irq_work)) {
					dev_info(pcm_dev->dev, "cancel IRQ work\n");
					cancel_delayed_work(&pcm_dev->irqinfo.irq_work);
				}
				gpio_free(pcm_dev->irqinfo.irq_gpio);
				free_irq(pcm_dev->irqinfo.irq, pcm_dev);
			}
		if (pcm_dev->pcm_ctrl.pcmdevice_profile_controls) {
			for (mix_index = 0; mix_index <
				pcm_dev->pcm_ctrl.nr_controls; mix_index++)
				kfree(pcm_dev->pcm_ctrl.
					pcmdevice_profile_controls
					[mix_index].name);
			pcm_dev->pcm_ctrl.nr_controls = 0;
			devm_kfree(pcm_dev->dev,
				pcm_dev->pcm_ctrl.pcmdevice_profile_controls);
		}
		pcmdevice_config_info_remove(pcm_dev);
		mutex_unlock(&pcm_dev->codec_lock);
	}
}

static int pcmdevice_hw_params(struct snd_pcm_substream *substream,
			  struct snd_pcm_hw_params *params,
			  struct snd_soc_dai *dai)
{
	struct pcmdevice_priv *pcm_dev = snd_soc_dai_get_drvdata(dai);
	unsigned int fsrate;
	unsigned int slot_width;
	int bclk_rate;
	int rc = 0;

	dev_info(pcm_dev->dev, "%s: %s\n",
		__func__, substream->stream == SNDRV_PCM_STREAM_PLAYBACK?
		"Playback":"Capture");

	fsrate = params_rate(params);
	switch (fsrate) {
	case 48000:
		break;
	case 44100:
		break;
	default:
		dev_err(pcm_dev->dev,
			"%s: incorrect sample rate = %u\n",
			__func__, fsrate);
		rc = -EINVAL;
		goto out;
	}

	slot_width = params_width(params);
	switch (slot_width) {
	case 16:
		break;
	case 20:
		break;
	case 24:
		break;
	case 32:
		break;
	default:
		dev_err(pcm_dev->dev,
			"%s: incorrect slot width = %u\n",
			__func__, slot_width);
		rc = -EINVAL;
		goto out;
	}

	bclk_rate = snd_soc_params_to_bclk(params);
	if (bclk_rate < 0) {
		dev_err(pcm_dev->dev,
			"%s: incorrect bclk rate = %d\n",
			__func__, bclk_rate);
		rc = bclk_rate;
		goto out;
	}
	dev_info(pcm_dev->dev, "%s: BCLK rate = %d Channel = %d"
		"Sample rate = %u slot width = %u\n",
		__func__, bclk_rate, params_channels(params),
		fsrate, slot_width);
out:
	return rc;
}

static const struct snd_soc_component_driver
	soc_codec_driver_pcmdevice = {
	.probe			= pcmdevice_codec_probe,
	.remove			= pcmdevice_codec_remove,
	.dapm_widgets		= pcmdevice_dapm_widgets,
	.num_dapm_widgets	= ARRAY_SIZE(pcmdevice_dapm_widgets),
	.dapm_routes		= pcmdevice_audio_map,
	.num_dapm_routes	= ARRAY_SIZE(pcmdevice_audio_map),
	.suspend_bias_off	= 1,
	.idle_bias_on		= 0,
	.use_pmdown_time	= 1,
	.endianness		= 1,
#if KERNEL_VERSION(6, 5, 0) > LINUX_VERSION_CODE
	.non_legacy_dai_naming	= 1,
#endif
};

static int pcmdevice_set_dai_sysclk(struct snd_soc_dai *codec_dai,
	int clk_id, unsigned int freq, int dir)
{
	struct pcmdevice_priv *pcm_dev = snd_soc_dai_get_drvdata(codec_dai);

	dev_info(pcm_dev->dev,
		"%s: clk_id = %d, freq = %u, CLK direction %s\n",
		__func__, clk_id, freq,
		dir == PCMDEVICE_CLK_DIR_OUT ? "OUT":"IN");

	return 0;
}

static struct snd_soc_dai_ops pcmdevice_dai_ops = {
	.mute_stream = pcmdevice_mute,
	.hw_params = pcmdevice_hw_params,
	.set_sysclk = pcmdevice_set_dai_sysclk,
};

static struct snd_soc_dai_driver pcmdevice_dai_driver[] = {
	{
		.name = "pcmdevice-codec",
		.capture = {
			.stream_name	 = "Capture",
			.channels_min	 = 2,
			.channels_max	 = PCMDEVICE_MAX_CHANNELS,
			.rates		 = PCMDEVICE_RATES,
			.formats	 = PCMDEVICE_FORMATS,
		},
		.playback = {
			.stream_name	 = "Playback",
			.channels_min	 = 2,
			.channels_max	 = PCMDEVICE_MAX_CHANNELS,
			.rates		 = PCMDEVICE_RATES,
			.formats	 = PCMDEVICE_FORMATS,
		},
		.ops = &pcmdevice_dai_ops,
#if KERNEL_VERSION(6, 2, 0) <= LINUX_VERSION_CODE
		.symmetric_rate = 1,
#else
		.symmetric_rates = 1,
#endif
	}

};

#if defined(CONFIG_OF)
static const struct of_device_id pcmdevice_of_match[] = {
	{ .compatible = "ti,adc3120" },
	{ .compatible = "ti,adc3140" },
	{ .compatible = "ti,adc5120" },
	{ .compatible = "ti,adc5140" },
	{ .compatible = "ti,adc6120" },
	{ .compatible = "ti,adc6140" },
	{ .compatible = "ti,dix4192" },
	{ .compatible = "ti,pcm1690" },
	{ .compatible = "ti,pcm186x" },
	{ .compatible = "ti,pcm3120" },
	{ .compatible = "ti,pcm3140" },
	{ .compatible = "ti,pcm5120" },
	{ .compatible = "ti,pcm5140" },
	{ .compatible = "ti,pcm6120" },
	{ .compatible = "ti,pcm6140" },
	{ .compatible = "ti,pcm6240" },
	{ .compatible = "ti,pcm6260" },
	{ .compatible = "ti,pcm9211" },
	{ .compatible = "ti,pcmd3140" },
	{ .compatible = "ti,pcmd3180" },
	{ .compatible = "ti,pcm512x" },
	{ .compatible = "ti,taa5412" },
	{ .compatible = "ti,tad5212" },
	{},
};
MODULE_DEVICE_TABLE(of, pcmdevice_of_match);
#endif

#ifdef CONFIG_ACPI
static const struct acpi_device_id pcmdevice_acpi_match[] = {
	{ "ADC3120", ADC3120	},
	{ "ADC3140", ADC3140	},
	{ "ADC5120", ADC5120	},
	{ "ADC5140", ADC5140	},
	{ "ADC6120", ADC6120	},
	{ "ADC6140", ADC6140	},
	{ "DIX4192", DIX4192	},
	{ "PCM1690", PCM1690	},
	{ "PCM186X", PCM186X	},
	{ "PCM3120", PCM3120	},
	{ "PCM3140", PCM3140	},
	{ "PCM5120", PCM5120	},
	{ "PCM5140", PCM5140	},
	{ "PCM6120", PCM6120	},
	{ "PCM6140", PCM6140	},
	{ "PCM6240", PCM6240	},
	{ "PCM6260", PCM6260	},
	{ "PCM9211", PCM9211	},
	{ "PCMD3140", PCMD3140	},
	{ "PCMD3180", PCMD3180	},
	{ "PCM512X", PCM512X	},
	{ "TAA5412", TAA5412	},
	{ "TAD5212", TAD5212	},
	{},
};
MODULE_DEVICE_TABLE(acpi, pcmdevice_acpi_match);
#endif

#if KERNEL_VERSION(6, 5, 0) >= LINUX_VERSION_CODE
static int pcmdevice_i2c_probe(struct i2c_client *i2c,
	const struct i2c_device_id *id)
#else
static int pcmdevice_i2c_probe(struct i2c_client *i2c)
#endif
{
#if KERNEL_VERSION(6, 5, 0) < LINUX_VERSION_CODE
	const struct i2c_device_id *id = i2c_match_id(pcmdevice_i2c_id, i2c);
#endif
	struct pcmdevice_priv *pcm_dev;
	const struct acpi_device_id *acpi_id;
	unsigned int dev_addrs[MAX_DEV_NUM];
	int ret = 0, i = 0, ndev = 0;
	bool isacpi = true;

	pcm_dev = devm_kzalloc(&i2c->dev, sizeof(*pcm_dev), GFP_KERNEL);
	if (!pcm_dev) {
		dev_err(&i2c->dev,
			"probe devm_kzalloc failed %d\n", ret);
		ret = -ENOMEM;
		goto out;
	}

	if (ACPI_HANDLE(&i2c->dev)) {
		acpi_id = acpi_match_device(i2c->dev.driver->acpi_match_table,
				&i2c->dev);
		if (!acpi_id) {
			dev_err(&i2c->dev, "No driver data\n");
			ret = -EINVAL;
			goto out;
		}
		pcm_dev->chip_id = acpi_id->driver_data;
	} else {
		pcm_dev->chip_id = (id != NULL) ? id->driver_data : 0;
		isacpi = false;
	}

	pcm_dev->dev = &i2c->dev;
	pcm_dev->client = i2c;

	if (pcm_dev->chip_id >= MAX_DEVICE)
		pcm_dev->chip_id = 0;

	pcm_dev->regmap = devm_regmap_init_i2c(i2c,
		&pcmdevice_i2c_regmap);
	if (IS_ERR(pcm_dev->regmap)) {
		ret = PTR_ERR(pcm_dev->regmap);
		dev_err(&i2c->dev, "Failed to allocate register map: %d\n",
			ret);
		goto out;
	}

	if (isacpi) {
		ndev = device_property_read_u32_array(&i2c->dev,
			"ti,audio-devices", NULL, 0);
		if (ndev <= 0) {
			ndev = 1;
			dev_addrs[0] = i2c->addr;
		} else {
			ndev = (ndev < ARRAY_SIZE(dev_addrs))
				? ndev : ARRAY_SIZE(dev_addrs);
			ndev = device_property_read_u32_array(&i2c->dev,
				"ti,audio-devices", dev_addrs, ndev);
		}

		pcm_dev->irqinfo.irq_gpio =
			acpi_dev_gpio_irq_get(ACPI_COMPANION(&i2c->dev), 0);
	} else {
		struct device_node *np = pcm_dev->dev->of_node;
#ifdef CONFIG_OF
		const __be32 *reg, *reg_end;
		int len, sw, aw;

		aw = of_n_addr_cells(np);
		sw = of_n_size_cells(np);
		if (sw == 0) {
			reg = (const __be32 *)of_get_property(np,
				"reg", &len);
			reg_end = reg + len/sizeof(*reg);
			ndev = 0;
			do {
				dev_addrs[ndev] = of_read_number(reg, aw);
				reg += aw;
				ndev++;
			} while (reg < reg_end);
		} else {
			ndev = 1;
			dev_addrs[0] = i2c->addr;
		}
#else
		ndev = 1;
		dev_addrs[0] = i2c->addr;
#endif
		pcm_dev->irqinfo.irq_gpio = of_irq_get(np, 0);
	}

	for(i = 0; i < ndev; i++) {
		pcm_dev->pcmdevice[i].addr = dev_addrs[i];
		dev_info(pcm_dev->dev, "%s = 0x%02x",
			dts_tag[i], pcm_dev->pcmdevice[i].addr);
	}
	pcm_dev->ndev = ndev;

	pcm_dev->reset = devm_gpiod_get_optional(&i2c->dev,
			"reset-gpios", GPIOD_OUT_HIGH);
	if (IS_ERR(pcm_dev->reset))
		dev_err(&i2c->dev, "%s ERROR: Can't get reset GPIO\n",
			__func__);

	if (PCM1690 == pcm_dev->chip_id)
		goto skip_interrupt;
	if (gpio_is_valid(pcm_dev->irqinfo.irq_gpio)) {
		dev_dbg(pcm_dev->dev, "irq-gpio = %d",
			pcm_dev->irqinfo.irq_gpio);
		INIT_DELAYED_WORK(&pcm_dev->irqinfo.irq_work,
			irq_work_routine);

		ret = gpio_request(pcm_dev->irqinfo.irq_gpio, "AUDEV-IRQ");
		if (!ret) {
			gpio_direction_input(pcm_dev->irqinfo.irq_gpio);

			pcm_dev->irqinfo.irq = gpio_to_irq(pcm_dev->irqinfo.irq_gpio);
			dev_info(pcm_dev->dev, "irq = %d\n", pcm_dev->irqinfo.irq);

			ret = request_threaded_irq(pcm_dev->irqinfo.irq,
				pcmdevice_irq_handler, NULL, IRQF_TRIGGER_FALLING |
				IRQF_ONESHOT, pcm_dev->client->name, pcm_dev);
			if (!ret)
				disable_irq_nosync(pcm_dev->irqinfo.irq);
			else
				dev_err(pcm_dev->dev,
					"request_irq failed, %d\n",
					ret);
		} else
			dev_err(pcm_dev->dev, "%s: GPIO %d request error\n", __func__,
				pcm_dev->irqinfo.irq_gpio);
	} else {
		dev_err(pcm_dev->dev, "Looking up irq-gpio property"
			" failed %d\n", pcm_dev->irqinfo.irq_gpio);
		ret = -1;
	}

	if (ret != 0)
		goto skip_interrupt;

	switch (pcm_dev->chip_id) {
	case ADC3120:
	case ADC3140:
	case ADC5120:
	case ADC5140:
	case ADC6120:
	case ADC6140:
	case PCM3120:
	case PCM5120:
	case PCM6120:
		pcm_dev->irq_work_func =
			adc5120_irq_work_func;
		break;
	case PCM186X:
		pcm_dev->irq_work_func =
			pcm186x_irq_work_func;
		break;
	case PCM3140:
	case PCM5140:
	case PCM6140:
	case PCM6240:
		pcm_dev->irq_work_func =
			pcm6240_irq_work_func;
		break;
	case PCM6260:
		pcm_dev->irq_work_func =
			pcm6260_irq_work_func;
		break;
	case PCM9211:
		pcm_dev->irq_work_func =
			pcm9211_irq_work_func;
		break;
	case PCMD3140:
	case PCMD3180:
		pcm_dev->irq_work_func =
			pcmd31x0_irq_work_func;
		break;
	case TAA5412:
		pcm_dev->irq_work_func =
			taa5412_irq_work_func;
		break;
	}

skip_interrupt:
	mutex_init(&pcm_dev->dev_lock);
	mutex_init(&pcm_dev->codec_lock);

	i2c_set_clientdata(i2c, pcm_dev);

	ret = sysfs_create_group(&pcm_dev->dev->kobj,
				&pcmdevice_attribute_group);
	if (ret < 0) {
		dev_err(&i2c->dev, "Sysfs registration failed\n");
		goto out;
	}

	ret = devm_snd_soc_register_component(&i2c->dev,
		&soc_codec_driver_pcmdevice,
		pcmdevice_dai_driver, ARRAY_SIZE(pcmdevice_dai_driver));
	if (ret < 0)
  		dev_err(&i2c->dev,
			"probe register component failed %d\n", ret);

out:
	return ret;
}

#if KERNEL_VERSION(6, 5, 0) >= LINUX_VERSION_CODE
static int pcmdevice_i2c_remove(struct i2c_client *i2c)
#else
static void pcmdevice_i2c_remove(struct i2c_client *i2c)
#endif
{
	struct pcmdevice_priv *pcm_dev = i2c_get_clientdata(i2c);

	if (pcm_dev) {
		sysfs_remove_group(&pcm_dev->dev->kobj,
			&pcmdevice_attribute_group);
		mutex_destroy(&pcm_dev->dev_lock);
		mutex_destroy(&pcm_dev->codec_lock);
	}
#if KERNEL_VERSION(6, 5, 0) >= LINUX_VERSION_CODE
	return 0;
#endif
}

static int pcmdevice_pm_suspend(struct device *dev)
{
	struct pcmdevice_priv *pcm_dev = dev_get_drvdata(dev);

	if (!pcm_dev) {
		dev_err(pcm_dev->dev, "%s: drvdata is NULL\n", __func__);
		return -EINVAL;
	}

	mutex_lock(&pcm_dev->codec_lock);

	pcm_dev->runtime_suspend = true;

	if (pcm_dev->irq_work_func != NULL) {
		if (delayed_work_pending(&pcm_dev->irqinfo.irq_work)) {
			dev_dbg(pcm_dev->dev, "cancel IRQ work\n");
			cancel_delayed_work_sync(&pcm_dev->irqinfo.irq_work);
		}
	}
	mutex_unlock(&pcm_dev->codec_lock);
	return 0;
}

static int pcmdevice_pm_resume(struct device *dev)
{
	struct pcmdevice_priv *pcm_dev = dev_get_drvdata(dev);

	if (!pcm_dev) {
		dev_err(pcm_dev->dev, "%s: drvdata is NULL\n", __func__);
		return -EINVAL;
	}

	mutex_lock(&pcm_dev->codec_lock);
	pcm_dev->runtime_suspend = false;
	mutex_unlock(&pcm_dev->codec_lock);
	return 0;
}

static const struct dev_pm_ops pcmdevice_pm_ops = {
	.suspend = pcmdevice_pm_suspend,
	.resume = pcmdevice_pm_resume
};

MODULE_DEVICE_TABLE(i2c, pcmdevice_i2c_id);

static struct i2c_driver pcmdevice_i2c_driver = {
	.driver = {
		.name	= "pcmdevice-codec",
		.owner = THIS_MODULE,
		.of_match_table = of_match_ptr(pcmdevice_of_match),
		.acpi_match_table = ACPI_PTR(pcmdevice_acpi_match),
		.pm = &pcmdevice_pm_ops,
	},
	.probe		= pcmdevice_i2c_probe,
	.remove 	= pcmdevice_i2c_remove,
	.id_table	= pcmdevice_i2c_id,
};

module_i2c_driver(pcmdevice_i2c_driver);

MODULE_AUTHOR("Shenghao Ding <shenghao-ding@ti.com>");
MODULE_DESCRIPTION("ASoC PCMDEVICE Driver");
MODULE_LICENSE("GPL");
